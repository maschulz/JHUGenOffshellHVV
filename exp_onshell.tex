We start by investigating the on-shell production and decay of the \Hboson with its coupling to either
weak or strong vector bosons in the VBF and ggH processes. 
There has already been extensive study of the $HVV$ couplings, 
and the current challenge is in the measurement of multiple possible anomalous contributions. 
On the other hand, there have been limited studies of the anomalous $Hgg$ couplings, due to 
lower statistical precision at this time. The latter could be interpreted as both an effective coupling 
to gluons, or as a coupling to quarks in the gluon fusion loop. 
Prospects of both $HVV$ and $Hgg$ studies with either 3000 $fb^{-1}$ (HL-LHC) or 300 $fb^{-1}$ (full LHC) 
are presented below. Let us first discuss some general features in analysis of LHC data. 

For the $HVV$ studies, we will use the example of the $H\to VV\to4\ell$ decay and VBF, $VH$, or ggH production.
Equation~(\ref{eq:HVV}) defines several anomalous couplings, which we generically denote as $g_i^{VV}$.
% and we define the set of cross-section fractions $f_{gn}$ using Eq.~(\ref{eq:fgn}).
All of these processes include the interference of several $VV$ intermediate states, 
such as ${VV}=ZZ, Z\gamma, \gamma\gamma, WW$.
%It is convenient to parameterize anomalous couplings as their effective contribution to the cross section,
%%%and the corresponding phases relative to the dominant tree-level contribution as 
%%%%%%%%%%%%%%%%%%%%%%%
%\begin{equation}
%f^{VV}_{gi} = \frac{|{g^{VV}_i}|^2 \sigma^{VV}_{i} \cdot \cos\phi^{VV}_{gi} }
%{|{g^{VV}_1}|^2 \sigma^{VV}_{1} + |{g^{VV}_2}|^2 \sigma^{VV}_{2} + |{g^{VV}_4}|^2 \sigma^{VV}_{4} +\ldots}
%\,,
%~~~{\rm where}~\phi^{VV}_{gi} = {\rm arg}(g^{VV}_i/g^{VV}_1)
%\,,
%\label{eq:fa3}
%\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%
%where in the case of real couplings $g_i$ we absorb the relative sign $\cos\phi^{VV}_{gi} = \pm1$
%into the definition of $f^{VV}_{gi}$. 
%The $\sigma^{VV}_{i}$ are constants, which can be selected by convention and are chosen 
%to be the cross sections of the $H\to VV\to2e2\mu$ process corresponding to the $g^{VV}_{i} = 1$ 
%contribution in decay, with all other $g^{VV}_{j}=0$ and without the production cross section.
%For example, $\sigma^{ZZ}_4/\sigma^{ZZ}_1=6.53$. 
%This definition covers the $VV=ZZ, Z\gamma, \gamma\gamma$ couplings, while the $WW$ couplings
%depend on these and do not require dedicated
%parameters. It is also possible to define the $f^{VV}_{gi}$ cross-section fractions for another process
%by calculating $\sigma^{VV}_{i}$ for that process.  We have already used $f^{\rm VBF}_{g4}$ 
%in Fig.~\ref{fig:fa3vbf} and Section~\ref{sect:exp_kinematics}.
%Our primary convention, however, relies on the decay cross sections, which are independent of
%parton distribution functions and other similar effects with relatively large uncertainties.
%The definition in Eq.~(\ref{eq:fa3}) makes the $f^{VV}_{gi}$ invariant with respect to the coupling constant convention. 
%For example, if we rescale the $g^{VV}_{i}$ couplings by any arbitrary constant factors, 
%including those in Eq.~(\ref{eq:EFT_ci}), the cross section contributions $f^{VV}_{gi}$ will remain the same. 
%In the case of the $Z\gamma$ and $\gamma\gamma$ couplings, we introduce the requirement 
%$\sqrt{|q_i^2|}>4$\,GeV in the cross-section calculation to avoid infrared divergence. 
%
%In the case of the $Hgg$ couplings, the only two contributions are $g_2^{gg}$ and $g_4^{gg}$.  We define 
%%%%%%%%%%%%%%%%%%%%%%%
%\begin{equation}
%f^{gg}_{\rm CP} = \frac{|{g^{gg}_4}|^2 \cdot \cos\phi^{gg}_{\rm CP}}
%{|{g^{gg}_2}|^2 + |{g^{gg}_4}|^2}
%\,,
%~~~{\rm where}~\phi^{gg}_{\rm CP} = {\rm arg}(g^{gg}_4/g^{gg}_2)
%\,,
%\label{eq:fa3gg}
%\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%
%where the cross sections drop out from the ratio because $\sigma^{gg}_{2}=\sigma^{gg}_{4}$. 
%
In the analysis of the data (MC simulation in our case), a likelihood fit is performed~\cite{Verkerke:2003ir,Brun:1997pa}.
The probability density function for a given signal process, before proper normalization, 
is defined for the two possible numbers of couplings $N$ in the product:
%%%%%%%%%%%%%%%%%%%%%%%
\begin{eqnarray}
N=4: ~~~ &&
\mathcal{P} \left({\bf x}; \vec{f} \,\right) 
=\sum_{\substack{k,l,m,n=1\\k\le l\le m\le n}}^K
\mathcal{P}_{klmn}\left({\bf x}\right)
\sqrt{|f_{gk} \cdot f_{gl}\cdot f_{gm}\cdot f_{gn}|} ~\mathrm{sign}(f_{gk} \cdot f_{gl}\cdot f_{gm}\cdot f_{gn})\,,
\label{eq:psignal}
\\
N=2: ~~~ &&
\mathcal{P} \left({\bf x}; \vec{f} \,\right) 
=\sum_{\substack{k,l=1\\k\le l}}^K
\mathcal{P}_{kl}\left({\bf x}\right)
\sqrt{|f_{gk} \cdot f_{gl}|} ~\mathrm{sign}(f_{gk} \cdot f_{gl})\,,
\label{eq:psignalshorter}
\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%%%%
where  ${\bf x}$ are the observables, but not necessarily the complete set ${\bf\Omega}$,
and $f_{gn}$ are $K$ terms corresponding to the cross-section fractions of the couplings,
defined in Eq.~(\ref{eq:fgn}).
Equations~(\ref{eq:psignal}) and~(\ref{eq:psignalshorter}) are obtained from Eq.~(\ref{eq:diff-cross-section2})
and using Eq.~(\ref{eq:an}), where the width and $f_{g1}$ are absorbed into the overall normalization. 
In the case of the electroweak process, the $HVV$ coupling appears on both the production and the decay sides.
As a result, the amplitude squared has a product of $N=4$ couplings. 
%This is the situation discussed in application to Eq.~(\ref{eq:poffshellACsimplified}), but expressed through 
%the $f_{gi}$ and the observables ${\bf x}$, which are not necessarily the complete set ${\bf\Omega}$.
In the gluon fusion production, on the other hand, the electroweak $HVV$ couplings appear only in decay, 
and therefore $N=2$. Similarly, if one considers the $Hgg$ coupling on production, $N=2$. 

There are $(N+K-1)!/(N!(K-1)!)$ terms in either Eq.~(\ref{eq:psignal}) or Eq.~(\ref{eq:psignalshorter}).
As we explain in Section~\ref{HVV_onshell}, we consider $K=5$ in our analysis of four anomalous $HVV$ couplings.
Therefore, in the case of electroweak production ($N=4$, $K=5$), we have to deal with 70 terms. 
If we were to consider $K=13$ independent couplings in Eq.~(\ref{eq:HVV}), we would formally have to deal 
with 1820 terms describing production and decay (the actual number would be somewhat smaller because 
not all terms contribute to a given decay mode). 
% N=4, K=5: 8!/(4!4!) = 8*7*6*5 / (4*3*2) = 70
% N=4, K=13: 16!/(12!4!) = 16*15*14*13 / (4*3*2) = 1820
% N=4, K=9: 12!/(8!4!) = 12*11*10*9 / (4*3*2) = 495
% N=4, K=8: 11!/(7!4!) = 11*10*9*8 / (4*3*2) = 330
While such analysis of 1820 terms is in principle feasible, at the current stage it is not practical. 
In the case of gluon fusion, there are 15 terms for $HVV$ couplings ($N=2$, $K=5$)
and 3 terms for $Hgg$ couplings ($N=2$, $K=2$). 
If both sets of anomalous couplings are considered simultaneously,
the total number of terms is the product of these, that is 45.

In the simplified analysis of LHC data, using simulation, we adopt the following approach. We take the analysis 
of the $H\to VV\to4\ell$ channel as the most interesting for illustration, because both production and decay 
information can be used. Inclusion of the $H\to\gamma\gamma$, $H\to\tau\tau$, and $H\to bb$ channels 
might increase the dataset by about an order of magnitude, but only for analysis of the production information. 
In addition, analysis of the $H\to WW\to2\ell2\nu$ decay may bring some information on the decay side, 
but not exceeding that from the $H\to 4\ell$ case. While we focus on the $H\to 4\ell$ channel, we comment 
on improvements which will be achieved with a combination of channels. 
The LHC events at $pp$ collision energy of 13 TeV are generated with the JHU generator discussed
in Section~\ref{sect:cp_mc}, except for the dominant $q\bar{q}\to 4\ell$ background process which is generated with 
POWHEG~\cite{powhegvv} and scaled to cover for other possible background contributions not modeled otherwise. 
All events are passed through Pythia~8~\cite{Sjostrand:2014zea} for parton shower simulation. 
The detector effects are modeled with ad-hoc acceptance selection, and the lepton and 
hadronic jet momenta are smeared to achieve realistic resolution effects. 

%====================================================================

\subsection{HVV anomalous couplings}
\label{HVV_onshell}

In order to illustrate the power of the matrix element techniques and the analysis tools discussed above, 
let us consider the $HVV$ coupling of the \Hboson to two weak vector bosons 
using the $H\to 4\ell$ decay,  with vector boson fusion, associated production with the vector bosons 
$W$ and $Z$, or inclusive production, and using both \onshell\ and \offshell\ production. 
% Though, we will leave techniques unique to the off-shell production to the next Section. 
Some of these techniques have already been applied in analyses of LHC 
data~\cite{Sirunyan:2017tqd,Sirunyan:2019twz,Sirunyan:2019nbs}.
However, the rich kinematics in production and decay of the \Hboson represents particular
challenges in analysis. 

There are 13 independent $HVV$ anomalous couplings in Eq.~(\ref{eq:HVV}).  An optimal simultaneous 
measurement of all these couplings, or even a sizable subset, represents a practical challenge in data analysis 
and, as far as we know, has not been attempted experimentally yet. Here we stress that an optimal measurement 
means that the precision of any given parameter measurement is not degraded when comparing a multi-parameter 
approach with all other couplings constrained and an optimal single-parameter measurement discussed below. 
Several approaches have been adopted. 
In one approach, a small number of couplings, typically two or at most three, is considered. 
One of these is the SM-like coupling and the other could be parameterized with the cross-section fraction 
$f_{gi}$ defined above. 
While this approach is optimal for each parameter measurement, 
the problem with this approach is that correlations between measurements 
of different anomalous couplings are not considered. 

Another recently adopted approach is the STXS measurement, where cross sections of several 
\Hboson production processes are measured in several bins based on kinematics of the event. 
While this approach is attractive due to its applicability to a number of various use cases, 
the problems with this approach are that observables are not necessarily 
optimal for any given measurement, and that the kinematics of events are assumed to follow the SM when measuring
the cross section in each bin. For a correct measurement, a full detector simulation of each coupling scenario is needed, 
because the kinematics of associated particles and decay products would affect the measurement in each bin.
The STXS approach based on SM-only kinematics does not include these effects.
The latter effect is especially important because neglecting it may lead to biases in the measurements. 
In the following, we illustrate the strengths and weaknesses of each approach,
and propose a practical method based on the matrix element approach. 

First, we would like to note that it is difficult to perform an unambiguous measurement of all
13 independent $HVV$ anomalous couplings in Eq.~(\ref{eq:HVV}) in a given process. For example, 
while all these couplings contribute to the VBF production, kinematics of $WW$ and $ZZ$ fusion 
are essentially identical, as shown in Fig.~\ref{fig:kinematics_wbf}. The measurement becomes feasible
when the $WW$ and $ZZ$ couplings are related. We adopt two examples of this relationship. 
In one case, we simply set $g_i^{WW} = g_i^{ZZ}$, which could be interpreted as relationships in 
Eqs.~(\ref{eq:deltaMW}--\ref{eq:kappa1WW}) under the $c_w=1$ condition. Such results could be
re-interpreted for a different relationship of the couplings.
In the second case, we adopt the relationships in Eqs.~(\ref{eq:deltaMW}--\ref{eq:kappa2Zgamma})
without any conditions. With such a simplification, we are still left with nine parameters in the first case 
and eight parameters in the second case. To simplify the analysis further, 
we reduce the number of free parameters by setting 
$g_2^{\gamma\gamma}=g_4^{\gamma\gamma}=g_2^{Z\gamma}=g_4^{Z\gamma}=0$. 
While we do expect to observe non-zero values of $g_2^{\gamma\gamma}$ and $g_2^{Z\gamma}$
even in the SM, constraints on all four couplings are possible from decays $H\to \gamma\gamma$ 
and $Z\gamma$ with on-shell photons. We leave the exercise to include all couplings in an optimal 
analysis to future studies. In addition, we keep the $g_2^{gg}$ and $g_4^{gg}$ couplings as two 
free parameters as well. While the dedicated studies of these couplings are presented in Section~\ref{ggHJJ},
kinematics of the ggH process may affect measurements in the VBF process. 

As a reference, we take the STXS stage-1.1 binning as applied by the CMS 
experiment~\cite{CMS-HIG-19-001, deFlorian:2016spz}. In this approach, seven event categories are defined, 
which are optimal  for separating the VBF topology with two associated jets; two $VH$ categories, with leptonic and 
hadronic decay of the $V$, respectively; the VBF topology with one associated jet; two $\ttH$ categories, with 
leptonic and fully hadronic top decay, respectively; and the untagged category, which includes the rest of the events.
We call it stage-0 categorization. 
Each category of events is further split into sub-categories to match the requirements on the transverse 
momenta and invariant masses, as defined in the STXS stage-1.1 binning. In total, there are 22 categories 
defined~\cite{CMS-HIG-19-001}. 
While the above STXS stage-1.1 categorization provides fine binning for capturing some kinematic features
in production of the \Hboson, it does not keep any information from decay, it has no information sensitive 
to $CP$ violation, and more generally, it is not guaranteed to be optimal for measuring any of the 
parameters of our interest. 

Since we target the optimal analysis of four anomalous couplings expressed
through\footnote{There is an additional factor of $(-1)$ in the definition of $f_{\Lambda1}$ and $f_{\Lambda1}^{Z\gamma}$
following the convention in experimental measurements~\cite{Sirunyan:2019twz}.}
$f_{g4}$, $f_{g2}$, $f_{\Lambda1}$,  and $f_{\Lambda1}^{Z\gamma}$, we build the analysis in the following way. 
Instead of STXS stage-1.1 binning, we start from the seven categories
defined in stage-0 for isolating different event topologies. Since in this analysis we do not target fermion 
couplings\footnote{For a study of fermion couplings with this technique, see Ref.~\cite{Gritsan:2016hjl}.}, 
the two $\ttH$ categories are merged with the untagged category.
There are four discriminants relevant for this analysis, as defined by Eq.~(\ref{eq:melaAlt}): 
${\cal D}_{g4}$, ${\cal D}_{g2}$, ${\cal D}_{\Lambda1}$, and ${\cal D}_{\Lambda1}^{Z\gamma}$.
In addition, two interference discriminants, ${\cal D}_{\rm CP}$ and ${\cal D}_{\rm int}$, are defined by Eq.~(\ref{eq:melaInt}) 
for the $g_4$ and $g_2$ couplings, respectively. The two other interference discriminants are found to 
provide little additional information due to large correlations with the discriminants defined in Eq.~(\ref{eq:melaAlt}). 
The full available information is used in calculating the discriminants in the following way.
In the untagged category, the $H\to VV\to4\ell$ information is used.
In addition, the transverse momentum of the \Hboson is included, because it is sensitive to production. 
In both the VBF and $VH$ topologies with two associated jets, both production and decay information are used,
except for the two interference discriminants, where production information is chosen because it dominates. 
In the leptonic $VH$ category and the VBF topology with one associated jet, where information is in general missing,
the transverse momentum of the \Hboson is used, with finer binning than in the untagged category.
In the end, for each event in a category $j$ a set of observables ${\bf x}$ is defined. 

To parameterize the 70 terms in Eq.~(\ref{eq:psignal}) or the 15 terms in Eq.~(\ref{eq:psignalshorter}),
we rely on samples generated with JHUGen.  However, it is not necessary to generate 70 or 15 separate samples.
Instead, we generate a few samples that adequately cover the phase space and re-weight those samples using the
MELA package to parameterize the other terms.
To populate the probability distributions, we use a simulation of unweighted events with detector modeling,
and small statistical fluctuations are inevitable.
A critical step in the process is to ensure that even with these statistical fluctuations, the probability
density function $\mathcal{P}$, defined in Eqs.~(\ref{eq:psignal}) and (\ref{eq:psignalshorter}), remains positive
for all possible values of $\vec{f}$.  We detect negative probability by minimizing $\mathcal{P}$, which is a
polynomial in $\sqrt{|f_{gi}|}\cdot\mathrm{sign}(f_{gi})$. In the case of Eq.~(\ref{eq:psignal}), where the polynomial is quartic, we use
the Hom4PS program~\cite{HomotopyContinuation,Hom4PS1,Hom4PS2} to accomplish this minimization.  If negative
probability is possible, we modify $\mathcal{P}_{klmn}$ or $\mathcal{P}_{kl}$ using the cutting planes
algorithm~\cite{cuttingplanes}, using the Gurobi program~\cite{gurobi} in each iteration of the procedure,
until $\mathcal{P}$ is always positive.  We find that only small modifications to $\mathcal{P}_{klmn}$
or $\mathcal{P}_{kl}$ are needed.

%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\centering
\includegraphics[width=0.35\textwidth]{plots/scanfa3_6part.pdf}
\includegraphics[width=0.35\textwidth]{plots/scanfa2_6part.pdf}
\includegraphics[width=0.35\textwidth]{plots/scanfL1_6part.pdf}
\includegraphics[width=0.35\textwidth]{plots/scanfL1Zg_6part.pdf}
%\includegraphics[width=0.25\textwidth]{plots/scanfa3.pdf}
%\includegraphics[width=0.25\textwidth]{plots/scanfa3_medium.pdf}
%\includegraphics[width=0.25\textwidth]{plots/scanfa3_zoom.pdf}
%\includegraphics[width=0.25\textwidth]{plots/scanfa2.pdf}
%\includegraphics[width=0.25\textwidth]{plots/scanfa2_medium.pdf}
%\includegraphics[width=0.25\textwidth]{plots/scanfa2_zoom.pdf}
%\includegraphics[width=0.25\textwidth]{plots/scanfL1.pdf}
%\includegraphics[width=0.25\textwidth]{plots/scanfL1_medium.pdf}
%\includegraphics[width=0.25\textwidth]{plots/scanfL1_zoom.pdf}
%\includegraphics[width=0.25\textwidth]{plots/scanfL1Zg.pdf}
%\includegraphics[width=0.25\textwidth]{plots/scanfL1Zg_medium.pdf}
%\includegraphics[width=0.25\textwidth]{plots/scanfL1Zg_zoom.pdf}
\captionsetup{justification=raggedright}
\captionsetup{justification=centerlast}
%\captionsetup{justification=justified}
\caption{
Expected constraints from a simultaneous fit of $f_{g4}$, $f_{g2}$, $f_{\Lambda1}$, and $f_{\Lambda1}^{Z\gamma}$
using associated production and $\PH\to4\ell$ decay with 3000 (300)\,fb$^{-1}$ data.
% where the sign of the real couplings is incorporated with the $\cos\phi_i$ term. 
% The left plots also indicate expectation with 300\,fb$^{-1}$ with the numbers in parentheses on the $y$-axis. 
% The constraints on each parameter are shown with the other parameters describing the $HVV$ and $Hgg$ couplings profiled,
% including the overall signal strength. 
% The right two plots show the zoomed version of the plot on the left. 
Three analysis scenarios are shown: using MELA observables with production and decay (or decay only) information, and using STXS binning. 
The dashed horizontal lines show the 68 and 95\% CL regions.
}
\label{fig:fg4_scan}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%

In Fig.~\ref{fig:fg4_scan} we show the expected constraints on the four parameters of interest
$f_{g4}$, $f_{g2}$, $f_{\Lambda1}$,  and $f_{\Lambda1}^{Z\gamma}$, 
using both associated production and $\PH\to4\ell$ decay with 3000\,fb$^{-1}$ (or 300\,fb$^{-1}$) 
of data at a single LHC experiment. 
The constraints on each parameter are shown with the other parameters describing the $HVV$ and $Hgg$ couplings profiled,
including the overall signal strength. The MC scenario has been generated with the SM expectation.
The production information dominates in all constraints. However, as discussed in Section~\ref{subsec:unitarization},
this is due to unbounded growth of anomalous couplings with $q^2$. Since this behavior cannot 
continue forever, it is still interesting to look at the decay-only constraints, which do not rely 
on the $q^2$-dependent growth of the amplitude. Therefore, in Fig.~\ref{fig:fg4_scan} both kinds of
constraints are shown for illustration of the two limiting cases.  We point out that form factor scaling,
such as introduced in Eq.~(\ref{eq:formfact-spin0}), can be used for continuous study of this effect. 

In addition, a comparison is made to the approach where instead of the 
optimal discriminants, the STXS stage-1.1 bins are used as observables, while using full
simulation of all processes otherwise. There is a significant difference in expected precision.
The most striking effect is the lack of constraints from decay information, but there is a loss 
in precision using production information in STXS as well. 
It is interesting to point that there is still weak decay-related information in the categories
used in the STXS approach, because interference between identical leptons produces different rates of $2e2\mu$
events compared to $4e$ and $4\mu$, depending on the couplings. 

%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
	\centering
	\includegraphics[width=0.24\linewidth]{plots/scang1.pdf}
	\includegraphics[width=0.24\linewidth]{plots/scang2.pdf}
	\includegraphics[width=0.24\linewidth]{plots/scang1prime2.pdf}
	\includegraphics[width=0.24\linewidth]{plots/scang4.pdf}
	%\captionsetup{justification=raggedright}
	\captionsetup{justification=centerlast}
	%\captionsetup{justification=justified}
	\caption{
Expected constraints from a simultaneous fit of (from left to right) $\delta c_z$, $c_{zz}$, $c_{z \Box}$, and $\tilde c_{zz}$
using associated production and $\PH\to4\ell$ decay with 3000\,fb$^{-1}$ data.
The EFT coupling constraints are the result of re-interpretation from the signal strength and $f_{gi}$ 
measurements discussed in text. 
The constraints on each parameter are shown with the other parameters describing the $HVV$ and $Hgg$ couplings profiled. 
Two analysis scenarios are shown: using MELA observables and using STXS binning. 
The dashed horizontal lines show the 68 and 95\% CL regions.
	}
	\label{fig:eft_1Dscan}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
\centering
\includegraphics[width=0.32\textwidth]{plots/2D_g1_g2.pdf}
\includegraphics[width=0.32\textwidth]{plots/2D_g1_g1prime2.pdf}
\includegraphics[width=0.32\textwidth]{plots/2D_g1_g4.pdf}
\includegraphics[width=0.32\textwidth]{plots/2D_g2_g1prime2.pdf}
\includegraphics[width=0.32\textwidth]{plots/2D_g2_g4.pdf}
\includegraphics[width=0.32\textwidth]{plots/2D_g1prime2_g4.pdf}
%\captionsetup{justification=raggedright}
\captionsetup{justification=centerlast}
%\captionsetup{justification=justified}
\caption{
Expected two-dimensional constraints from a simultaneous fit of $\delta c_z$, $c_{zz}$, $c_{z \Box}$, and $\tilde c_{zz}$
as shown in Fig.~\ref{fig:eft_1Dscan} for the MELA observables. 
The constraints on each parameter are shown with the other parameters describing the $HVV$ and $Hgg$ couplings profiled. 
Top-left: $(\delta c_z, c_{zz})$; 
top-middle: $(\delta c_z, c_{z \Box})$;
top-right: $(\delta c_z, \tilde c_{zz})$; 
bottom-left: $(c_{zz}, c_{z \Box})$; 
bottom-middle: $(c_{zz}, \tilde c_{zz})$; 
bottom-right: $(c_{z \Box}, \tilde c_{zz})$.
}
\label{fig:eft_2Dscan}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%

Since $f_{gi}$ measurements involve ratios of couplings, most systematic uncertainties that would otherwise
affect the cross section measurements cancel in the ratio. 
Therefore, the $f_{gi}$ measurements are still expected to be statistics limited with 3000\,fb$^{-1}$ of data. 
For this reason, the expected results can be easily reinterpreted for another scenario of integrated luminosity, 
as for example the expectation with 300\,fb$^{-1}$ shown in parentheses. 
However, when the $f_{gi}$ measurements are re-interpreted in terms of
couplings (as we illustrate below), both the signal strength and the $f_{gi}$ results need to be combined. This leads
to sizable systematic uncertainties affecting the couplings. In the following, we assign 5\% theoretical and
5\% experimental uncertainties on the measurements of the signal strength, which is the ratio of the measured and expected
cross sections. 

We also perform a fit with three cross-section fraction parameters $f_{g4}$, $f_{g2}$, and $f_{\Lambda1}$
with the EFT relationship among couplings following Eqs.~(\ref{eq:deltaMW}--\ref{eq:kappa2Zgamma}). 
The conclusions of this study are similar to those presented above. 
We re-interpret these results as constraints on the $\delta c_z$, $c_{zz}$, $c_{z \Box}$, and $\tilde c_{zz}$
couplings, defined in the EFT parameterization in the Higgs basis.  
This fit requires reinterpreting the process cross section and the three fractions 
in terms of couplings, and one has to take dependence of the width on the couplings into account, 
following Eq.~(\ref{eq:diff-cross-section2}).
We assume that $\Gamma_{\rm other}=0$ and express the width using Eq.~(\ref{eq:width}).
The values of $\kappa_f=\kappa_t=\kappa_b=\kappa_\tau=\kappa_\mu$ and 
$\tilde \kappa_f=\tilde\kappa_t=\tilde\kappa_b=\tilde\kappa_\tau=\tilde\kappa_\mu$ 
are left unconstrained independently for the CP-even and CP-odd fermion couplings. 
The resulting one-dimensional constraints are shown in Fig.~\ref{fig:eft_1Dscan} and 
two-dimensional contours with the other parameters profiled are shown in Fig.~\ref{fig:eft_2Dscan}.
In Fig.~\ref{fig:eft_1Dscan} it is evident again that analysis based on the optimal discriminants provides the
best constraints on the couplings of interest. 

% Sanity check of these two lines:
% fg4 < 0.0002 (fig.10) at 95% CL 
% czz~ < 0.30 (fig.11) at 95% CL
%
% g4/g1 = sqrt(fg4/fg1)*sqrt(sigma1/sigma4) ~  sqrt(0.0002)*sqrt (6.53) =  0.036
% g1=2 => g4 < 0.072  (+- theory uncertainties) ====> as Heshy comments, this becomes <0.087 in EFT 
% alpha = e^2/4pi => 1/e^2=137/4pi=10.9; 
% sin^2(\thetaW)=0.231 => sW^2*cW^2=((1-0.231)*0.231)= 0.177639
% czz~ = 2*sW^2*cW^2*g4/e^2
% czz~ < 0.072 *2*0.177639*10.9  = 0.28   ====> as Heshy comments, this becomes <0.034 in EFT
%
% comment from Heshy: use rg4=cW^2 to correct f_g4 in EFT using Eq.(6) here: https://arxiv.org/pdf/1901.00174.pdf
% 1/(fg4+1) = (1/fg4m+1)(sigma4ZZ+rg4^2*sigma4WW)/(sigma4ZZ+sigma4WW)




%\subsection{CP studies in gluon fusion}
\subsection{Hgg anomalous couplings}
\label{ggHJJ}

%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\centering
\includegraphics[width=0.42\textwidth]{plots/d_diffjj.pdf}
\includegraphics[width=0.55\textwidth]{plots/d_2j_qqgen.pdf}
%\captionsetup{justification=raggedright}
\captionsetup{justification=centerlast}
%\captionsetup{justification=justified}
\caption{Left: The 2D distribution of the difference between the scalar and pseudoscalar populations of events 
for the $D_{2\rm jet}$ and $D_{0-}^{\rm ggH}$ discriminants, both self-normalized to 1, respectively. 
Right: $D_{0-}^{\rm ggH}$ discriminant distributions of the scalar and pseudoscalar population events 
with the requirement on the $D_{2\rm jet}$ discriminant below or above 0.5. 
} 
\label{fig:d0m_ggH}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%

The gluon fusion process in association with two jets allows analysis of kinematic distributions for
the measurement of potential anomalous contributions to the gluon fusion loop. Resolving the loop
effects is a separate task, which we do not attempt to perform in this work. However, we point out that 
unless the particles in the loop are light, their mass does not significantly affect the kinematics of the \Hboson 
and associated jets. The main effect is on the \Hboson's transverse momentum~\cite{deFlorian:2016spz}.
Our analysis of the CP properties of this interaction depends primarily on the angular kinematics
of the associated jets and \Hboson, as discussed in Section~\ref{sect:kin_vbf}, and the effects of the 
transverse momentum effects are small.
Therefore, in the rest of this work we treat the gluon fusion process without resolving the loop
contribution, allowing for any particles to contribute, either from SM or beyond. The only observable 
difference in this analysis is between the CP-even and CP-odd couplings, which can be
parameterized as the overall strength of the \Hboson's coupling to gluons and the fraction
 %of the CP-odd contribution $f_{g4}^{\rm ggH}$ defined in Eq.~(\ref{eq:fgn}).
 of the CP-odd contribution $f_{\rm CP}^{\rm gg}$ defined in Eq.~(\ref{eq:fgn}).
%The latter may be equivalently expressed through an effective mixing angle as $f_{g4}^{\rm ggH}=\sin^2(\alpha^{\rm ggH})$. 

The analysis strategy follows the approach discussed in application to the $HVV$ measurements in the previous section, 
with the difference being the two-jet category optimized for the measurement of the gluon fusion process. In addition to
a discriminant optimal for signal over background separation, the events are described by three observables. 
The $D_{2\rm jet}$ observables follows Eq.~(\ref{eq:melaAlt}), with the VBF and gluon fusion matrix elements 
used to isolate the VBF topology. The $D_{0-}^{\rm ggH}$ and $D_{\rm CP}^{\rm ggH}$ observables follow 
Eq.~(\ref{eq:melaAlt}) and Eq.~(\ref{eq:melaInt}) for separating the SM-like coupling and CP-odd coupling, but with 
one modification to the process definition. Only the quark-initiated process defines the matrix element in these two 
formulas, because only such a VBF-like topology of the gluon fusion process carries relevant CP information. 
This is illustrated in Fig.~\ref{fig:d0m_ggH}, where the left plot shows that the $D_{0-}^{\rm ggH}$ discriminant starts
to separate the two couplings at higher values of $D_{2\rm jet}$, which correspond to more VBF-like topology. 
The right plot shows that only at higher values of $D_{2\rm jet}$ can one observe the separation.  Only a small 
fraction of the total gluon fusion events end up in that region. This illustrates the challenge of the CP analysis in the gluon
fusion process. The $D_{\rm CP}^{\rm ggH}$ leads to forward-backward asymmetry in the distribution of events in the 
case of CP violation, when both CP-odd and CP-even amplitudes contribute. 

%Application of the two discriminants defined in Eq.~(\ref{eq:melaAlt}) and Eq.~(\ref{eq:melaInt}) to the 
%$f_{g4}^{\rm ggH}$ measurement in the the gluon fusion production at LHC 
%utilizing kinematics of the two associated jets is shown in Fig.~\ref{fig:fa3ggH}. 
%
%The separation between CP-even and CP-odd is known to increase with $m_{jj}$. 
% Further investigation to the event topologies is performed in the study and shown in 
% Fig.~\ref{fig:d0m_ggH}. It is interesting to see that the events produced by $qq$ initial 
% states are more powerful to determine the CP than the $gg$ initial state events. 
% Thus we define a discriminant $D_{0-}^{ggH, qq}$ that is sensitive to the qq initial state: 

%%%%%%%%%%%%%%%%%%%%%%%
%\begin{eqnarray}
%D_{0-}^{ggH, qq}= \frac{{\cal P}_{\rm 0+}^{qq}({\bf\Omega})}{{\cal P}_{\rm 0+}^{qq}({\bf\Omega})+{\cal P}_{\rm 0-}^{qq}({\bf\Omega})}
%\,,
%\label{eq:fracqq}
%\end{eqnarray}
%%%%%%%%%%%%%%%%%%%%%%%
%where ${\cal P}_{\rm 0+ qq}({\bf\Omega})$ and ${\cal P}_{\rm 0- qq}({\bf\Omega})$ stand for the probability 
% of the event produced through qq initial state under the 0+ and 0- assumption. The distribution of the
% $D_{0-}^{ggH, qq}$ discriminant is shown in Fig.~\ref{fig:d0m_ggH}. It is also interesting to notice that 
% the separation increases with $D_{2j}^{VBF}$. 

%%%%%%%%%%%%%%%%%%%%%%%
%\begin{figure}[h]
%\centering
%\includegraphics[width=0.45\textwidth]{plots/d_2jgen.pdf}
%\includegraphics[width=0.45\textwidth]{plots/d_2j_qqgen.pdf}
%\caption{Left: The $D_{0-}^{ggH}$ discriminant distribution with for events from different initial states. No cut on $m_{jj}$ is applied. Right: The $D_{a3}^{ggH, qq}$ discriminant distribution.} 
%\label{fig:d0m_ggH}
%\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%

%Jets are reconstructed after PYTHIA hadronization. Events are put in two: 2-jets category ($>= 2$ jets) 
% and untagged category ($< 2$ jets). The jets with highest $p_T$ are selected. The 2-jets category is what we are i
% nterested in, and the untagged is useful to constrain the overall ggH rate. We use $D_{bkg}$ in the untagged
% category to separate the signal from the qqZZ background. In the 2-jet category, 3 discriminants are used: 
% $D_{bkg}$, $D_{0-}^{ggH,qq}$ and $D_{2j}^{VBF}$. 
% As shown above, $D_{0-}^{ggH,qq}$ and $D_{2j}^{VBF}$ are particularly sensitive to $f_{a3}^{ggH}$. 
% Fig.~\ref{fig:fa3ggH_scan} shows the $D_{0-}^{ggH,qq}$ distribution after event selection in the 2-jets category. 

A projection of $f_{\rm CP}^{\rm gg}$ sensitivity with 3000 and 300\,fb$^{-1}$ at an LHC experiment is performed. 
The overall normalization of the gluon fusion production rate in the VBF-like topology is provided by the untagged events
and events with two associated jets in a non-VBF topology. The electroweak VBF process is a background to the 
 $f_{\rm CP}^{\rm gg}$ measurement in this case, but its kinematics are still distinct enough to keep it separated in the 
fit on the statistical basis. Keeping its CP properties unconstrained has little effect on the CP analysis in the gluon fusion process. 
We use the $H\to 4\ell$ analysis to illustrate the sensitivity, but scale the expected constraints with an effective luminosity 
ratio to account for the relative sensitivity of the $H\to \gamma\gamma$ and $\tau\tau$ channels based on the typical
sensitivity in the VBF topology~\cite{Sirunyan:2017tqd,Sirunyan:2019nbs,Sirunyan:2018koj,Aad:2019mbh}. 
%This ratio is roughly equivalent to four times larger luminosity compared to analysis in a single channel. 
The expected constraints are shown in Fig.~\ref{fig:fa3ggH_scan}. 
With 3000 (300)\,fb$^{-1}$, one can separate CP-even and CP-odd $Hgg$ couplings with a confidence level
of about 9\,(3)\,$\sigma$.
%4.5 (1.4) $\sigma$ and 8.9 (2.8) $\sigma$.

%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\centering
\includegraphics[width=0.45\textwidth]{plots/fa3ggHscan_writeup_new.pdf}
\includegraphics[width=0.45\textwidth]{plots/kappa_eft.pdf}
%\captionsetup{justification=raggedright}
\captionsetup{justification=centerlast}
%\captionsetup{justification=justified}
\caption{
Expected constraints on $f_{\rm CP}^{\rm gg}$ with 3000 (300)\,fb$^{-1}$  (left) 
and $\kappa_f$ and $\tilde \kappa_f$ couplings in the gluon fusion loop with 300\,fb$^{-1}$ (right),
using the $H\to4\ell, \gamma\gamma,$ and $\tau\tau$ decays.
The dashed horizontal lines (left) and contours (right) show the 68 and 95\% CL regions.
}
\label{fig:fa3ggH_scan}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%

We re-interpret these expected constraints on $f_{\rm CP}^{\rm gg}$ and the cross section 
as constraints on the $\kappa_f=\kappa_t=\kappa_b$ and 
$\tilde \kappa_f=\tilde\kappa_t=\tilde\kappa_b$ couplings in the gluon fusion loop, 
assuming that only the SM top and bottom quarks dominate the loop.   
There are additional considerations when re-interpreting the signal strength and $f_{\rm CP}^{\rm gg}$
in terms of couplings following Eq.~(\ref{eq:diff-cross-section2}). 
As in the $HVV$ measurements in Section~\ref{HVV_onshell}, 
we assume $\Gamma_{\rm other}=0$ and express the width $\Gamma_{\rm tot}$ using Eq.~(\ref{eq:width}).
%Second, when we consider three different channels $H\to4\ell, \gamma\gamma,$ and $\tau\tau$,
%the signal strength depends on the decay rate, which in turn depends on the loop contribution in the 
%$H\to\gamma\gamma$ decay, fermion couplings in the $H\to\tau\tau$ decay, and vector boson couplings in the 
%$H\to4\ell$ decay. Therefore, we choose to relate cross sections in the three channels using 
%Eqs.~(\ref{eq:ratio-1}), (\ref{eq:ratio-3}), and~(\ref{eq:ratio-5}), which include the parameters
%of interest $\kappa_f$ and $\tilde \kappa_f$. 
In this approach, the overall signal strength of the VBF and VH processes, proportional to $g_1^2$, remains unconstrained. 
Using the $f_{\rm CP}^{\rm gg}$ and gluon fusion cross section constraints expected with 300\,fb$^{-1}$ of data at LHC
with the $H\to4\ell, \gamma\gamma,$ and $\tau\tau$ decays, we show the expected constraints on 
$(\kappa_f, \tilde \kappa_f)$ in Fig.~\ref{fig:fa3ggH_scan} (right). The sign ambiguity 
$(\kappa_f, \tilde \kappa_f)\leftrightarrow(-\kappa_f, -\tilde\kappa_f)$ remains unresolved with experimental data,
but the relative sign of $\kappa_f$ and $\tilde \kappa_f$ can be resolved, due to the $D_{\rm CP}^{\rm ggH}$ observable. 
The measurement of the gluon fusion cross section alone leads to an elliptical constraint in the 2D likelihood scan, 
with the eccentricity determined by the ratio $\sigma(\tilde\kappa_f=1)/\sigma(\kappa_f=1)$ discussed earlier. 
The $f_{\rm CP}^{\rm gg}$ measurement leads to constraints within the ellipse.

Should sizable CP violation effects be hidden in the gluon fusion loop, they can be uncovered with the
HL-LHC data sample. Further improvements in the CP constraints on $(\kappa_f, \tilde \kappa_f)$ can be obtained
by measuring the $\ttH$ process, where even stronger constraints are expected~\cite{Gritsan:2016hjl}. 
However, we would like to point out that the ratio $\sigma(\tilde\kappa_f=1)/\sigma(\kappa_f=1)=0.39$ in the $\ttH$ process
is by a factor of six different from the ggH process. This large difference will lead to stronger constraints in the combination
of the two measurements under assumption of the top quark dominance in the loop
because of additional information from the ratio of cross sections. Nonetheless, the measurement 
in the gluon fusion is not limited to the top quark Yukawa coupling, but may include other BSM effects in the loop.
Therefore, it is possible for CP effects to show up in the ggH measurement, but not in $\ttH$. 
