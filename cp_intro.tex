% 
% LHC has established basic properties of Higgs. 
% Higgs discovered in all channels. 
% 
% how many events in each channel?
% 
% many typical deviations are smaller than current precision
% 
% Open questions: CP property. Other particles strongly coupling to higgs (light or heavy). 
% yukawa interactions. 
% 
% now and future: study off-shell tail. search for additional particles 
% in loops (--> couplings) and for new resonance. 

We present a coherent framework for the measurement of couplings of the Higgs ($H$) boson and a possible second spin-zero resonance. 
Our framework includes a Monte Carlo generator and matrix element techniques for optimal analysis of the data. We build upon the earlier
developed framework of the JHU generator and MELA analysis package~\cite{Gao:2010qx,Bolognesi:2012mm,Anderson:2013afp,Gritsan:2016hjl}
and extensively use matrix elements provided by MCFM~\cite{Campbell:2010ff,Campbell:2011bn,Campbell:2013una,Campbell:2015vwa,Campbell:2015qma}.
Thanks to the transparent implementation of standard model (SM) processes in MCFM, we extend them to add the most general scalar and 
gauge couplings and possible additional states. This allows us to build on the previously studied 
topics~\cite{Nelson:1986ki,Soni:1993jc,Plehn:2001nj,Choi:2002jk,Buszello:2002uu,Hankele:2006ma,Accomando:2006ga,
Godbole:2007cn,Hagiwara:2009wt,Gao:2010qx,DeRujula:2010ys,Christensen:2010pf,Bolognesi:2012mm,Ellis:2012xd,Chen:2012jy,
Artoisenet:2013puc,Anderson:2013afp,Chen:2013waa,Maltoni:2013sma,
Azatov:2014jga,Cacciapaglia:2014rla,Denner:2014cla,Dolan:2014upa,Englert:2014ffa,Gonzalez-Alonso:2014eva,
Ballestrero:2015jca,Greljo:2015sla,Hespel:2015zea,Kauer:2015dma,Kauer:2015hia,Kilian:2015opv,Mimasu:2015nqa,
Degrande:2016dqg,Dwivedi:2016xwm,Gritsan:2016hjl,deFlorian:2016spz,
Denner:2017vms,Deutschmann:2017qum,Greljo:2017spw,Goncalves:2017gzy,Jager:2017owh,
Brass:2018hfw,Gomez-Ambrosio:2018pnl,Goncalves:2018pkt,Harlander:2018yns,Harlander:2018yio,Lee:2018fxj,Kalinowski:2018oxd,Perez:2018kav,
Denner:2019fcr,Jaquier:2019bfs} and present phenomenological results in a unified approach.
This framework includes many options for production and decay of the $H$ boson.
Here we consider gluon fusion (ggH), vector boson fusion (VBF), and associated production with 
a vector boson ($VH$) in both \onshell\ $H$ and \offshell\ $H^*$ production, with decays to two vector bosons.
In the \offshell\ case, interference with background processes is included. 
Additional heavy particles in the gluon fusion loop and a second resonance interfering with the SM processes are also considered. 
In the $VH$ process, we include next-to-leading order QCD corrections, as well as the gluon fusion process for $ZH$.
The processes with direct sensitivity to fermion $\Hff$ couplings, such as $t\bar{t}H$, $b\bar{b}H$, $tqH$, or $H\to\tau^+\tau^-$,
are discussed in Ref.~\cite{Gritsan:2016hjl}.
%The processes initiated by the direct fermion $\Hff$ couplings within this framework are discussed elsewhere~\cite{Gritsan:2016hjl}.

In an earlier version of our framework, we focused mostly on the Run-I targets and their possible extensions as
documented in Refs.~\cite{Gao:2010qx,Bolognesi:2012mm,Anderson:2013afp}. It was adopted in Run-I 
analyses using Large Hadron Collider (LHC) data~\cite{Chatrchyan:2012xdj,Chatrchyan:2012jja,Chatrchyan:2013mxa,Chatrchyan:2013iaa,
Aad:2013xqa,Khachatryan:2014iha,Khachatryan:2014ira,Khachatryan:2014kca,Khachatryan:2015mma,Khachatryan:2015cwa,
Aad:2015mxa,Khachatryan:2016tnr}.
Some new features in this framework have been reported earlier~\cite{deFlorian:2016spz} and have been used
for LHC experimental analyses. Most notably, this framework was employed in recent Run-II measurements of the 
\onshell\ $HVV$ anomalous couplings from the joint analysis of production and decay~\cite{Sirunyan:2017tqd,Sirunyan:2019nbs}, 
from the joint analysis of \onshell\ and \offshell\ \Hboson\ production~\cite{Sirunyan:2019twz}, in the search for a second
resonance in interference with the continuum background~\cite{Sirunyan:2018qlb,Sirunyan:2019pqw},
and in projections to future  \onshell\ and \offshell\ \Hboson\ measurements at the High Luminosity (HL) LHC~\cite{Cepeda:2019klc}. 
In this paper, we document, review, and highlight the new features critical for exploring the full Run-II dataset at the LHC
and preparing for Run-III and the HL-LHC. 
We also broaden the theoretical underpinning, allowing interpretation in terms of either anomalous couplings or 
an effective field theory (EFT) framework.

Both Run-I and Run-II of the LHC have provided a large amount of data on \Hboson properties 
and its interactions with other SM particles, as analyzed by the ATLAS and CMS experiments. 
The \Hboson has been observed in all accessible production channels, 
gluon fusion, weak vector boson fusion, $VH$ associated production, and top-quark associated 
production~\cite{Khachatryan:2016vau,Sirunyan:2018koj,Aad:2019mbh,
Sirunyan:2018kst,Aaboud:2018zhk,Sirunyan:2018hoz,Aaboud:2018urx},
and its production strength is consistent with the SM prediction within the uncertainties~\cite{deFlorian:2016spz}.
Also its decay channels into gauge bosons ($ZZ, WW, \gamma\gamma)$ have been observed and 
do not show significant deviations within the uncertainties
~\cite{Khachatryan:2016vau, %ATLAS+CMS Run1
Sirunyan:2018koj, % CMS comb
Aad:2019mbh}. % ATLAS comb
The fermionic interactions have been established for the third generation quarks ($t,b$) and the $\tau$ 
lepton~\cite{
Sirunyan:2018kst, % CMS VH(bb)
Aaboud:2018zhk, % ATLAS VH(bb)
Sirunyan:2018hoz, %CMS ttH
Aaboud:2018urx, % ATLAS ttH
Sirunyan:2017khh, % CMS HTT
Aaboud:2018pen}, % ATLAS HTT
and so far, they are consistent with the SM within the uncertainties. 

While this picture shows that Nature does not radically deviate from the SM dynamics, 
it should be noted that many generic extensions of the SM predict deviations  
only below the current precision.
Open questions remain, for example about CP-odd mixtures, the Yukawa coupling hierarchy, and other 
states involved in electroweak symmetry breaking. 
These questions can be addressed in the years to come by fully utilizing the existing and upcoming LHC data sets. 
In particular, the study of kinematic tails of distributions involving the \Hboson is becoming accessible for the first time. 
These signals involve off-shell \Hboson production and strong interference effects with irreducible backgrounds 
that are subject to the electroweak unitarization mechanism in the SM.
This feature turns the kinematic tails into particularly sensitive probes of the mechanism of electroweak symmetry breaking 
and possible extensions beyond the SM. 
Moreover, the study of electroweak production of the \Hboson  (VBF and $VH$)
% vector boson fusion 
is probing $HVV$ interactions over a large range of momentum transfer, 
which can expose possible new particles that couple through loops. 
Even the direct production of new resonances will first show up as deviations from the expected high-energy tail of kinematic distributions. 
Hence, analyzing these newly accessible features in off-shell \Hboson production is 
of paramount importance to understand electroweak symmetry breaking in the SM and 
possible extensions involving new particles. 
In the following, we review the framework and demonstrate its capabilities through examples of possible analyses.

% % % The old text below is too much focused on the second resonance. (probably because of the 750GeV excitement)

% 
% The new $H$ boson with mass around 125 GeV discovered by the ATLAS and CMS 
% experiments~\cite{Chatrchyan:2012xdj,Aad:2012tfa} on LHC appears 
% to be consistent with a fundamental scalar particle with quantum numbers 
% $J^{PC}=0^{++}$~\cite{Khachatryan:2014kca,Aad:2015mxa}, 
% as predicted for an excitation of a scalar field responsible for generating
% mass of elementary particles~\cite{}. However, assignment of the quantum numbers assumes 
% $C\!P$ conservation in the $H$ boson interactions. One of the goals of the future measurements of the 
% $H(125)$ boson properties is to establish its $C\!P$ properties, or more generally constrain any possible 
% small anomalous contributions in its interactions with the other standard model particles. 
% More generally, should a new resonance be discovered on LHC, the same measurements should
% be applied there as well. 
% 
% Fusion of two strong or weak bosons, $gg$, $\gamma\gamma$, $Z\gamma$, $ZZ$, or $WW$,
% is one of the most promising mechanisms for production of new resonances on LHC.
% These are the dominant production mechanisms, and the same final states are
% experimentally favored decay mechanisms of the $H(125)$ boson, see Fig.~\ref{fig:feynman1}.
% These are also the promising channels to look for new states. 
% If the resonance width is sizable, interference with background, 
% illustrated in Fig.~\ref{fig:feynman2}, becomes important and one has to investigate the implications 
% for the observed kinematics. This effect is also important for the $H(125)$ boson decaying to $ZZ$ and $WW$
% with sizable fraction of events expected above the threshold for two on-shell vector bosons, the so-called
% off-shell $H(125)$ production. 

%->

% %%%%%%%%%%%%%%%%%%%%%%%
% \begin{figure}[t]
% \centerline{
% \includegraphics[width=0.6\linewidth]{plots/diagram_VVXVV.pdf}
% }
% \caption{
% Feynman diagram describing production of a resonance $X$ in fusion of two vector bosons 
% and its decay to two vector bosons, including $gg$, $\gamma\gamma$, $Z\gamma$, $ZZ$, and $WW$
% parings of strong and weak bosons. The blobs indicate effective couplings, either loop-induced or tree-level. 
% }
% \label{fig:feynman1}
% %\end{figure}
% %%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%
% %\begin{figure}[t]
% \centerline{
% \includegraphics[width=0.6\linewidth]{plots/diagram_VVVV.pdf}
% }
% \caption{
% Background to process shown in Fig.~\ref{fig:feynman1}. 
% The blob indicates an effective coupling, which may include an $H(125)$ propagator if production 
% of a second resonance $X$ is considered. 
% }
% \label{fig:feynman2}
% \end{figure}
%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%
% \begin{figure}[t]
% \centerline{
% \includegraphics[width=0.45\linewidth]{plots/diagram_HVV.pdf}
% }
% \vspace{-3.3cm}
% 
% \caption{
% Illustration of an effective $HVV$ coupling of a Higgs boson and vector bosons in the decay
% $H\to VV$, vector boson fusion $VV\to H$, and associate production with a vector boson $V\to VH$,
% where the arrows next to each process indicate time direction. 
% Both on-shell $H$ production and off-shell $H^*$ production are considered. 
% }
% \label{fig:feynman}
% \end{figure}
%%%%%%%%%%%%%%%%%%%%%%%


