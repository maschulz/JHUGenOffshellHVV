(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     37818,       1007]
NotebookOptionsPosition[     35977,        945]
NotebookOutlinePosition[     36487,        965]
CellTagsIndexPosition[     36444,        962]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[" "], "Input",
 CellChangeTimes->{3.788873558584347*^9}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"InsertNumbers", "=", 
   RowBox[{"{", "}"}]}], "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.788266594854704*^9, 3.788266595178507*^9}}],

Cell[BoxData[
 RowBox[{"{", "}"}]], "Output",
 CellChangeTimes->{3.788266595475094*^9, 3.788270557609414*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"InsertNumbers", "=", 
     RowBox[{"{", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"sw2", "\[Rule]", " ", "0.23119"}], ",", "\[IndentingNewLine]", 
       RowBox[{"cw2", "\[Rule]", " ", 
        RowBox[{"1", "-", "sw2"}]}], ",", "\[IndentingNewLine]", 
       RowBox[{"MZ2", "\[Rule]", " ", 
        RowBox[{"91.19", "^", "2"}]}], ",", "\[IndentingNewLine]", 
       RowBox[{"g2ZZ", "\[Rule]", " ", "0.1"}], ",", 
       "                                   ", 
       RowBox[{"(*", " ", 
        RowBox[{
        "choosing", " ", "g2ZZ", " ", "as", " ", "our", " ", "input", " ", 
         "parameter"}], " ", "*)"}], "\[IndentingNewLine]", 
       RowBox[{"Lam1WW", "\[Rule]", " ", "1000"}], ",", "\[IndentingNewLine]", 
       RowBox[{"Lam1ZZ", "\[Rule]", " ", "1000"}], ",", "\[IndentingNewLine]", 
       RowBox[{"Lam1ZA", "\[Rule]", " ", "1000"}], ",", "\[IndentingNewLine]", 
       RowBox[{"kap1ZZ", "\[Rule]", "0"}]}], " ", "\[IndentingNewLine]", 
      "}"}]}], ";"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"InsertNumbers2", "=", 
     RowBox[{"{", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"sw2", "\[Rule]", " ", "0.23119"}], ",", "\[IndentingNewLine]", 
       RowBox[{"cw2", "\[Rule]", " ", 
        RowBox[{"1", "-", "sw2"}]}], ",", "\[IndentingNewLine]", 
       RowBox[{"MZ2", "\[Rule]", " ", 
        RowBox[{"91.19", "^", "2"}]}], ",", "\[IndentingNewLine]", 
       RowBox[{"kap1ZZ", "\[Rule]", " ", "10"}], ",", 
       "                                   ", 
       RowBox[{"(*", " ", 
        RowBox[{
        "choosing", " ", "g2ZZ", " ", "as", " ", "our", " ", "input", " ", 
         "parameter"}], " ", "*)"}], "\[IndentingNewLine]", 
       RowBox[{"g2ZZ", "\[Rule]", "0"}], " ", ",", "\[IndentingNewLine]", 
       RowBox[{"Lam1WW", "\[Rule]", " ", "1000"}], ",", "\[IndentingNewLine]", 
       RowBox[{"Lam1ZZ", "\[Rule]", " ", "1000"}], ",", "\[IndentingNewLine]", 
       RowBox[{"Lam1ZA", "\[Rule]", " ", "1000"}]}], "\[IndentingNewLine]", 
      "}"}]}], ";"}], "\[IndentingNewLine]", 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.788179363344942*^9, 3.788179414696281*^9}, {
   3.788179470920656*^9, 3.788179501092791*^9}, {3.788179872715794*^9, 
   3.788179899563223*^9}, {3.788180358766158*^9, 3.788180370974433*^9}, {
   3.788180451982875*^9, 3.788180456094223*^9}, {3.7881808178407927`*^9, 
   3.7881808338716297`*^9}, {3.7881842902623863`*^9, 3.788184318075135*^9}, {
   3.7881843592367783`*^9, 3.7881843593323936`*^9}, {3.7881869448521757`*^9, 
   3.788186944994779*^9}, {3.7882664933063*^9, 3.78826649468184*^9}, 
   3.788266594282501*^9, {3.788266676075458*^9, 3.788266680154867*^9}, {
   3.788269597307089*^9, 3.788269597914792*^9}, {3.788270811442226*^9, 
   3.788270822082193*^9}, {3.788270931234262*^9, 3.788270940946518*^9}, {
   3.788271134067685*^9, 3.788271140915531*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{"Here", ",", " ", 
    RowBox[{"we", " ", "use", " ", "kap1WW"}], ",", 
    RowBox[{
    "kap2ZA", " ", "as", " ", "the", " ", "dependent", " ", "couplings", "  ",
      "and", " ", "g2ZZ", " ", "as", " ", "input"}]}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"eq1", "=", 
     RowBox[{"g2WW", "\[Equal]", 
      RowBox[{"cw2", " ", "*", " ", "g2ZZ"}]}]}], ";"}], 
   "                                                                          \
                                             ", 
   RowBox[{"(*", " ", 
    RowBox[{"EQ", ".18", " ", "in", " ", "our", " ", "paper"}], " ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"eq2", " ", "=", " ", 
     RowBox[{
      RowBox[{
       RowBox[{"kap1WW", "/", 
        RowBox[{"Lam1WW", "^", "2"}]}], " ", 
       RowBox[{"(", 
        RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", "\[Equal]", "   ", 
      RowBox[{"2", " ", "sw2", " ", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"-", "g2ZZ"}], ")"}], "/", "MZ2"}]}]}]}], ";"}], 
   "                                                ", 
   RowBox[{"(*", " ", 
    RowBox[{"EQ", ".20", " ", "in", " ", "our", " ", "paper"}], " ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"eq3", " ", "=", " ", 
     RowBox[{
      RowBox[{
       RowBox[{"kap2ZA", "/", 
        RowBox[{"Lam1ZA", "^", "2"}]}], " ", 
       RowBox[{"(", 
        RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", "\[Equal]", " ", 
      RowBox[{"2", " ", 
       RowBox[{"Sqrt", "[", 
        RowBox[{"sw2", " ", "*", " ", "cw2"}], "]"}], " ", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"-", "g2ZZ"}], ")"}], "/", "MZ2"}]}]}]}], ";"}], 
   "                       ", 
   RowBox[{"(*", " ", 
    RowBox[{"EQ", ".21", " ", "in", " ", "our", " ", "paper"}], " ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"sol1", "=", 
     RowBox[{"Solve", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"eq1", ",", "eq2", ",", "eq3"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"g2WW", ",", "kap1WW", ",", "kap2ZA"}], "}"}]}], "]"}]}], 
    ";"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"sol1", " ", "//.", " ", "InsertNumbers"}], 
   "                                                                          \
                                              ", 
   RowBox[{"(*", " ", 
    RowBox[{"insertnig", " ", "numbers"}], "  ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"%", "[", 
     RowBox[{"[", "1", "]"}], "]"}], " ", "//", "TableForm"}], 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.788179420731292*^9, 3.788179572313142*^9}, {
   3.788179729691556*^9, 3.788179839626759*^9}, {3.7881799051155*^9, 
   3.7881799204273853`*^9}, {3.788180258270235*^9, 3.788180350846187*^9}, {
   3.7881803820944843`*^9, 3.7881804150062637`*^9}, 3.788180596816657*^9, {
   3.788180651120327*^9, 3.788180687135989*^9}, {3.788180793728627*^9, 
   3.788180807345343*^9}, {3.7881810642747507`*^9, 3.788181064899283*^9}, {
   3.788182208345945*^9, 3.7881822888686447`*^9}, {3.7881832300629168`*^9, 
   3.788183239599086*^9}, {3.788184321814003*^9, 3.788184347300425*^9}, {
   3.788267355966742*^9, 3.788267373039304*^9}, 3.788270895394346*^9, 
   3.7882709437640247`*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"g2WW", "\[Rule]", "0.076881`"}], ",", 
    RowBox[{"kap1WW", "\[Rule]", 
     RowBox[{"-", "10.34258723579077`"}]}], ",", 
    RowBox[{"kap2ZA", "\[Rule]", 
     RowBox[{"-", "18.86054256469145`"}]}]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{{3.788179445676635*^9, 3.788179477891677*^9}, {
   3.788179507965496*^9, 3.7881795726126747`*^9}, {3.7881797860790358`*^9, 
   3.788179793122429*^9}, 3.788179840043652*^9, {3.788179902336547*^9, 
   3.788179921982252*^9}, {3.78818033657883*^9, 3.788180351484479*^9}, {
   3.788180408622657*^9, 3.7881804434789658`*^9}, 3.788180597256915*^9, 
   3.788180689034527*^9, {3.7881807947250338`*^9, 3.788180807734027*^9}, {
   3.788181043890297*^9, 3.7881810658906603`*^9}, {3.7881822197618837`*^9, 
   3.788182239759591*^9}, {3.788182279840331*^9, 3.788182289202052*^9}, 
   3.788183982221984*^9, {3.788184292437421*^9, 3.7881843616861277`*^9}, 
   3.788186949720903*^9, 3.788241577514407*^9, 3.788266353732596*^9, 
   3.7882696023987103`*^9, 3.788270561216117*^9, {3.788271117928207*^9, 
   3.788271143955421*^9}, 3.788271723945938*^9, 3.788271856251726*^9}],

Cell[BoxData[
 TagBox[
  TagBox[GridBox[{
     {
      RowBox[{"g2WW", "\[Rule]", "0.076881`"}]},
     {
      RowBox[{"kap1WW", "\[Rule]", 
       RowBox[{"-", "10.34258723579077`"}]}]},
     {
      RowBox[{"kap2ZA", "\[Rule]", 
       RowBox[{"-", "18.86054256469145`"}]}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.5599999999999999]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}],
   Column],
  Function[BoxForm`e$, 
   TableForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{{3.788179445676635*^9, 3.788179477891677*^9}, {
   3.788179507965496*^9, 3.7881795726126747`*^9}, {3.7881797860790358`*^9, 
   3.788179793122429*^9}, 3.788179840043652*^9, {3.788179902336547*^9, 
   3.788179921982252*^9}, {3.78818033657883*^9, 3.788180351484479*^9}, {
   3.788180408622657*^9, 3.7881804434789658`*^9}, 3.788180597256915*^9, 
   3.788180689034527*^9, {3.7881807947250338`*^9, 3.788180807734027*^9}, {
   3.788181043890297*^9, 3.7881810658906603`*^9}, {3.7881822197618837`*^9, 
   3.788182239759591*^9}, {3.788182279840331*^9, 3.788182289202052*^9}, 
   3.788183982221984*^9, {3.788184292437421*^9, 3.7881843616861277`*^9}, 
   3.788186949720903*^9, 3.788241577514407*^9, 3.788266353732596*^9, 
   3.7882696023987103`*^9, 3.788270561216117*^9, {3.788271117928207*^9, 
   3.788271143955421*^9}, 3.788271723945938*^9, 3.788271856262368*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.788266385958033*^9, 3.7882663860925694`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{"Here", ",", " ", 
    RowBox[{"we", " ", "use", " ", "kap1WW"}], ",", 
    RowBox[{
    "kap1ZZ", " ", "as", " ", "the", " ", "dependent", " ", "couplings", " ", 
     "and", " ", "g2ZZ", " ", "as", " ", "input"}]}], "  ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"eq1", "=", 
     RowBox[{"g2WW", "\[Equal]", 
      RowBox[{"cw2", " ", "*", " ", "g2ZZ"}]}]}], ";"}], 
   "                                                                          \
                                             ", 
   RowBox[{"(*", " ", 
    RowBox[{"EQ", ".18", " ", "in", " ", "our", " ", "paper"}], " ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"eq2", " ", "=", " ", 
     RowBox[{
      RowBox[{
       RowBox[{"kap1WW", "/", 
        RowBox[{"Lam1WW", "^", "2"}]}], " ", 
       RowBox[{"(", 
        RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", "\[Equal]", "  ", 
      RowBox[{
       RowBox[{"kap1ZZ", "/", 
        RowBox[{
         RowBox[{"(", "Lam1ZZ", ")"}], "^", "2"}]}], " ", "+", "  ", 
       RowBox[{"2", " ", "sw2", " ", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"-", "g2ZZ"}], ")"}], "/", "MZ2"}]}]}]}]}], ";"}], 
   "                                                ", 
   RowBox[{"(*", " ", 
    RowBox[{"EQ", ".20", " ", "in", " ", "our", " ", "paper"}], " ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"eq3", " ", "=", " ", 
     RowBox[{"0", " ", "\[Equal]", " ", 
      RowBox[{"2", " ", 
       RowBox[{"Sqrt", "[", 
        RowBox[{"sw2", " ", "*", " ", "cw2"}], "]"}], " ", 
       RowBox[{"(", "    ", 
        RowBox[{
         RowBox[{"kap1ZZ", "/", 
          RowBox[{
           RowBox[{"(", "Lam1ZZ", ")"}], "^", "2"}]}], " ", "+", " ", " ", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"-", "g2ZZ"}], ")"}], "/", "MZ2"}]}], ")"}]}]}]}], ";"}], 
   "                       ", 
   RowBox[{"(*", " ", 
    RowBox[{"EQ", ".21", " ", "in", " ", "our", " ", "paper"}], " ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"sol1", "=", 
     RowBox[{"Solve", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"eq1", ",", "eq2", ",", "eq3"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"g2WW", ",", "kap1ZZ", ",", "kap1WW"}], "}"}]}], "]"}]}], 
    ";"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"sol1", " ", "//.", " ", "InsertNumbers"}], 
   "                                                                          \
                                              ", 
   RowBox[{"(*", " ", 
    RowBox[{"insertnig", " ", "numbers"}], "  ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"%", "[", 
     RowBox[{"[", "1", "]"}], "]"}], " ", "//", "TableForm"}]}]}]], "Input",
 CellChangeTimes->{{3.788266379429103*^9, 3.788266474314262*^9}, {
   3.78826656349879*^9, 3.788266585626585*^9}, {3.78826663244346*^9, 
   3.788266638730888*^9}, {3.7882673507833347`*^9, 3.788267382895185*^9}, 
   3.788270893986364*^9, {3.788270945778707*^9, 3.788270946722391*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"g2WW", "\[Rule]", "0.03613407`"}], ",", 
    RowBox[{"kap1ZZ", "\[Rule]", "5.652016571568281`"}], ",", 
    RowBox[{"kap1WW", "\[Rule]", "5.652016571568281`"}]}], "}"}], 
  "}"}]], "Output",
 CellChangeTimes->{
  3.788266383975731*^9, {3.788266449726346*^9, 3.788266496509864*^9}, {
   3.7882665669273367`*^9, 3.788266597589539*^9}, {3.788266634641739*^9, 
   3.7882666392670193`*^9}, {3.7882666707195253`*^9, 3.788266682429194*^9}, 
   3.7882673512804728`*^9, {3.788267384193207*^9, 3.788267390240342*^9}}],

Cell[BoxData[
 TagBox[
  TagBox[GridBox[{
     {
      RowBox[{"g2WW", "\[Rule]", "0.03613407`"}]},
     {
      RowBox[{"kap1ZZ", "\[Rule]", "5.652016571568281`"}]},
     {
      RowBox[{"kap1WW", "\[Rule]", "5.652016571568281`"}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.5599999999999999]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}],
   Column],
  Function[BoxForm`e$, 
   TableForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{
  3.788266383975731*^9, {3.788266449726346*^9, 3.788266496509864*^9}, {
   3.7882665669273367`*^9, 3.788266597589539*^9}, {3.788266634641739*^9, 
   3.7882666392670193`*^9}, {3.7882666707195253`*^9, 3.788266682429194*^9}, 
   3.7882673512804728`*^9, {3.788267384193207*^9, 3.788267390240922*^9}}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.788268321105611*^9, 3.788268321267726*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{"Here", ",", " ", 
    RowBox[{"we", " ", "use", " ", "kap1WW"}], ",", " ", 
    RowBox[{
     RowBox[{"g2", "^", "ZA"}], " ", "as", " ", "the", " ", "dependent", " ", 
     "couplings", " ", "and", " ", "g2ZZ", " ", "as", " ", "input"}]}], "  ", 
   "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"eq1", "=", 
     RowBox[{"g2WW", "\[Equal]", 
      RowBox[{
       RowBox[{"cw2", " ", "*", " ", "g2ZZ"}], " ", "+", " ", 
       RowBox[{"2", " ", 
        RowBox[{"Sqrt", "[", 
         RowBox[{"sw2", " ", "cw2"}], "]"}], " ", "g2ZA"}]}]}]}], ";"}], 
   "                                                                          \
                                             ", 
   RowBox[{"(*", " ", 
    RowBox[{"EQ", ".18", " ", "in", " ", "our", " ", "paper"}], " ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"eq2", " ", "=", " ", 
     RowBox[{
      RowBox[{
       RowBox[{"kap1WW", "/", 
        RowBox[{"Lam1WW", "^", "2"}]}], " ", 
       RowBox[{"(", 
        RowBox[{"cw2", "-", "sw2"}], ")"}]}], "\[Equal]", "    ", 
      RowBox[{
       RowBox[{"2", " ", "sw2", " ", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"-", "g2ZZ"}], ")"}], "/", "MZ2"}]}], " ", "+", " ", 
       RowBox[{"2", " ", 
        RowBox[{"Sqrt", "[", 
         RowBox[{"sw2", "/", "cw2"}], "]"}], " ", 
        RowBox[{"(", 
         RowBox[{"cw2", "-", "sw2"}], ")"}], " ", 
        RowBox[{"g2ZA", "/", "MZ2"}]}]}]}]}], ";", 
    "                                                ", 
    RowBox[{"(*", " ", 
     RowBox[{"EQ", ".20", " ", "in", " ", "our", " ", "paper"}], " ", "*)"}], 
    "\[IndentingNewLine]", 
    RowBox[{"eq3", " ", "=", " ", 
     RowBox[{"0", " ", "\[Equal]", " ", 
      RowBox[{
       RowBox[{"2", " ", 
        RowBox[{"Sqrt", "[", 
         RowBox[{"sw2", " ", "*", " ", "cw2"}], "]"}], " ", 
        RowBox[{"(", "    ", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"-", "g2ZZ"}], ")"}], "/", "MZ2"}], ")"}]}], " ", "+", " ", 
       RowBox[{"2", "  ", 
        RowBox[{"(", 
         RowBox[{"cw2", "-", "sw2"}], ")"}], " ", 
        RowBox[{"g2ZA", "/", "MZ2"}]}]}]}]}], " ", ";", 
    "                       ", 
    RowBox[{"(*", " ", 
     RowBox[{"EQ", ".21", " ", "in", " ", "our", " ", "paper"}], " ", "*)"}], 
    "\[IndentingNewLine]", 
    RowBox[{"sol1", "=", 
     RowBox[{"Solve", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"eq1", ",", "eq2", ",", "eq3"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"g2WW", ",", "g2ZA", ",", "kap1WW"}], "}"}]}], "]"}]}], ";"}],
    "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"sol1", " ", "//.", " ", "InsertNumbers"}], 
   "                                                                          \
                                              ", 
   RowBox[{"(*", " ", 
    RowBox[{"insertnig", " ", "numbers"}], "  ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"%", "[", 
     RowBox[{"[", "1", "]"}], "]"}], " ", "//", "TableForm"}]}]}]], "Input",
 CellChangeTimes->{{3.788268329268036*^9, 3.788268332643797*^9}, {
  3.788268368292404*^9, 3.7882683803404503`*^9}, {3.788268443253022*^9, 
  3.788268493284987*^9}, {3.788268530469781*^9, 3.788268564855226*^9}, {
  3.7882708871702347`*^9, 3.788270890273941*^9}, {3.7882709484347143`*^9, 
  3.788270949858819*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"g2WW", "\[Rule]", "0.0672111714593951`"}], ",", 
    RowBox[{"g2ZA", "\[Rule]", "0.03685670238033562`"}], ",", 
    RowBox[{"kap1WW", "\[Rule]", 
     RowBox[{"-", "1.6274974338987016`*^-15"}]}]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{{3.788268497095641*^9, 3.788268565768675*^9}}],

Cell[BoxData[
 TagBox[
  TagBox[GridBox[{
     {
      RowBox[{"g2WW", "\[Rule]", "0.0672111714593951`"}]},
     {
      RowBox[{"g2ZA", "\[Rule]", "0.03685670238033562`"}]},
     {
      RowBox[{"kap1WW", "\[Rule]", 
       RowBox[{"-", "1.6274974338987016`*^-15"}]}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.5599999999999999]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}],
   Column],
  Function[BoxForm`e$, 
   TableForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{{3.788268497095641*^9, 3.788268565769863*^9}}]
}, Open  ]],

Cell[BoxData["  "], "Input",
 CellChangeTimes->{{3.78818041919777*^9, 3.788180424437141*^9}, {
  3.7881806919998417`*^9, 3.78818070590366*^9}, {3.7881807921939573`*^9, 
  3.788180797057144*^9}}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.788179590763486*^9, 3.788179594633273*^9}, {
  3.788180809775901*^9, 3.7881808099521313`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{"Here", ",", " ", 
    RowBox[{"we", " ", "use", " ", "kap1WW"}], ",", 
    RowBox[{
    "kap2ZA", " ", "as", " ", "the", " ", "dependent", " ", "couplings", "  ",
      "and", " ", "kap1ZZ", " ", "as", " ", "input"}]}], "  ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"eq2", " ", "=", " ", 
     RowBox[{
      RowBox[{
       RowBox[{"kap1WW", "/", 
        RowBox[{"Lam1WW", "^", "2"}]}], " ", 
       RowBox[{"(", 
        RowBox[{"cw2", "-", "sw2"}], ")"}]}], "\[Equal]", "  ", 
      RowBox[{"kap1ZZ", "/", 
       RowBox[{
        RowBox[{"(", "Lam1ZZ", ")"}], "^", "2"}]}]}]}], "  ", ";"}], 
   "                                                ", 
   RowBox[{"(*", " ", 
    RowBox[{"EQ", ".20", " ", "in", " ", "our", " ", "paper"}], " ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"eq3", " ", "=", " ", 
     RowBox[{
      RowBox[{
       RowBox[{"kap2ZA", "/", 
        RowBox[{"Lam1ZA", "^", "2"}]}], " ", 
       RowBox[{"(", 
        RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", "\[Equal]", "  ", 
      RowBox[{"2", " ", 
       RowBox[{"Sqrt", "[", 
        RowBox[{"sw2", " ", "*", " ", "cw2"}], "]"}], "    ", 
       RowBox[{"kap1ZZ", "/", 
        RowBox[{
         RowBox[{"(", "Lam1ZZ", ")"}], "^", "2"}]}]}]}]}], " ", ";"}], 
   "                       ", 
   RowBox[{"(*", " ", 
    RowBox[{"EQ", ".21", " ", "in", " ", "our", " ", "paper"}], " ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"sol1", "=", 
     RowBox[{"Solve", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"eq2", ",", "eq3"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"kap1WW", ",", "kap2ZA"}], "}"}]}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"sol1", " ", "//.", " ", "InsertNumbers2"}], 
   "                                                                          \
                                              ", 
   RowBox[{"(*", " ", 
    RowBox[{"insertnig", " ", "numbers"}], "  ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"%", "[", 
     RowBox[{"[", "1", "]"}], "]"}], " ", "//", "TableForm"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.788270672492291*^9, 3.788270829314106*^9}, {
  3.788270875522378*^9, 3.7882708838578453`*^9}, {3.78827177592737*^9, 
  3.788271776583065*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"kap1WW", "\[Rule]", "18.600498493359623`"}], ",", 
    RowBox[{"kap2ZA", "\[Rule]", "15.683703140568351`"}]}], "}"}], 
  "}"}]], "Output",
 CellChangeTimes->{{3.7882708034565353`*^9, 3.788270829733979*^9}, 
   3.788270898728314*^9, {3.788271754954501*^9, 3.788271777187771*^9}, 
   3.7882718301386433`*^9}],

Cell[BoxData[
 TagBox[
  TagBox[GridBox[{
     {
      RowBox[{"kap1WW", "\[Rule]", "18.600498493359623`"}]},
     {
      RowBox[{"kap2ZA", "\[Rule]", "15.683703140568351`"}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.5599999999999999]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}],
   Column],
  Function[BoxForm`e$, 
   TableForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{{3.7882708034565353`*^9, 3.788270829733979*^9}, 
   3.788270898728314*^9, {3.788271754954501*^9, 3.788271777187771*^9}, 
   3.78827183014643*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", "\[IndentingNewLine]", "\[IndentingNewLine]", 
  "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.7882706759971323`*^9, 3.788270676368577*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{"Now", ",", " ", 
    RowBox[{
    "we", " ", "calculate", " ", "the", " ", "TGC", " ", "and", " ", "QGC", 
     " ", "in", " ", "our", " ", "paper"}]}], " ", " ", "*)"}], 
  "\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"TGC", "=", " ", 
     RowBox[{"{", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"d1A", " ", "\[Rule]", "  ", 
        RowBox[{"1", " ", "-", " ", 
         RowBox[{"cw2", " ", "g2ZZ"}]}]}], ",", "\[IndentingNewLine]", 
       RowBox[{"d1Z", " ", "\[Rule]", "  ", 
        RowBox[{"1", "-", 
         RowBox[{"2", " ", "sw2", " ", 
          RowBox[{"cw2", "/", 
           RowBox[{"(", 
            RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
          RowBox[{"(", 
           RowBox[{"-", "g2ZZ"}], ")"}]}], "  ", "-", " ", 
         RowBox[{
          RowBox[{
           RowBox[{"MZ2", "/", "2"}], "/", 
           RowBox[{"(", 
            RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
          RowBox[{"kap1ZZ", "/", 
           RowBox[{"Lam1ZZ", "^", "2"}]}]}]}]}], ",", "\[IndentingNewLine]", 
       RowBox[{"d2Z", "\[Rule]", " ", 
        RowBox[{"1", "-", 
         RowBox[{
          RowBox[{"sw2", "/", 
           RowBox[{"(", 
            RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
          RowBox[{"(", 
           RowBox[{"-", "g2ZZ"}], ")"}]}], "-", " ", 
         RowBox[{
          RowBox[{
           RowBox[{"MZ2", "/", "2"}], "/", 
           RowBox[{"(", 
            RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
          RowBox[{"kap1ZZ", "/", 
           RowBox[{"Lam1ZZ", "^", "2"}]}]}]}]}], ",", "\[IndentingNewLine]", 
       RowBox[{"d3Z", "\[Rule]", " ", 
        RowBox[{"1", "-", 
         RowBox[{
          RowBox[{"sw2", "/", 
           RowBox[{"(", 
            RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
          RowBox[{"(", 
           RowBox[{"-", "g2ZZ"}], ")"}]}], "-", " ", 
         RowBox[{
          RowBox[{
           RowBox[{"MZ2", "/", "2"}], "/", 
           RowBox[{"(", 
            RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
          RowBox[{"kap1ZZ", "/", 
           RowBox[{"Lam1ZZ", "^", "2"}]}]}]}]}], ",", "\[IndentingNewLine]", 
       RowBox[{"dZZWW", "\[Rule]", " ", 
        RowBox[{
         RowBox[{"cw2", "/", "sw2"}], " ", 
         RowBox[{"(", 
          RowBox[{
           RowBox[{"2", " ", 
            RowBox[{"(", 
             RowBox[{"1", "-", 
              RowBox[{
               RowBox[{"sw2", "/", 
                RowBox[{"(", 
                 RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
               RowBox[{"(", 
                RowBox[{"-", "g2ZZ"}], ")"}]}]}], ")"}]}], "-", "1"}], 
          ")"}]}]}], ",", "\[IndentingNewLine]", 
       RowBox[{"dZAWW", "\[Rule]", " ", 
        RowBox[{
         RowBox[{"Sqrt", "[", 
          RowBox[{"cw2", "/", "sw2"}], "]"}], " ", 
         RowBox[{"(", 
          RowBox[{"1", "-", 
           RowBox[{
            RowBox[{"sw2", "/", 
             RowBox[{"(", 
              RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
            RowBox[{"(", 
             RowBox[{"-", "g2ZZ"}], ")"}]}]}], ")"}]}]}], ",", 
       "\[IndentingNewLine]", "\[IndentingNewLine]", 
       RowBox[{"d2A", "\[Rule]", " ", "1"}], ",", "\[IndentingNewLine]", 
       RowBox[{"d3A", "\[Rule]", "1"}], ",", "\[IndentingNewLine]", 
       "\[IndentingNewLine]", 
       RowBox[{"d4A", "\[Rule]", "0"}], ",", "\[IndentingNewLine]", 
       RowBox[{"d4Z", "\[Rule]", "0"}]}], "\[IndentingNewLine]", "}"}]}], 
    ";"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"TGC", "  ", "//.", " ", "InsertNumbers"}], " ", "//.", " ", 
     "sol1"}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"%", "[", 
     RowBox[{"[", "1", "]"}], "]"}], " ", "//", "TableForm"}]}]}]], "Input",
 CellChangeTimes->{
  3.7881804334694157`*^9, {3.788180845664703*^9, 3.78818108978059*^9}, {
   3.788182292441526*^9, 3.788182337258157*^9}, {3.7881832170864153`*^9, 
   3.788183242462572*^9}, {3.788184092915782*^9, 3.7881841058436813`*^9}, {
   3.78818414133187*^9, 3.788184148900051*^9}, {3.788184452822447*^9, 
   3.78818446021336*^9}, 3.788270915203068*^9, {3.788270977330702*^9, 
   3.788271033651146*^9}, 3.7882711227237587`*^9, 3.788271788439348*^9, 
   3.788271868119948*^9}],

Cell[BoxData[
 TagBox[
  TagBox[GridBox[{
     {
      RowBox[{"d1A", "\[Rule]", "0.923119`"}]},
     {
      RowBox[{"d1Z", "\[Rule]", "1.066121492466798`"}]},
     {
      RowBox[{"d2Z", "\[Rule]", "1.0430024924667982`"}]},
     {
      RowBox[{"d3Z", "\[Rule]", "1.0430024924667982`"}]},
     {
      RowBox[{"dZZWW", "\[Rule]", "3.61145158729529`"}]},
     {
      RowBox[{"dZAWW", "\[Rule]", "1.9019992247370465`"}]},
     {
      RowBox[{"d2A", "\[Rule]", "1"}]},
     {
      RowBox[{"d3A", "\[Rule]", "1"}]},
     {
      RowBox[{"d4A", "\[Rule]", "0"}]},
     {
      RowBox[{"d4Z", "\[Rule]", "0"}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.5599999999999999]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}],
   Column],
  Function[BoxForm`e$, 
   TableForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{
  3.7881810039177513`*^9, {3.7881810364990664`*^9, 3.7881810900962477`*^9}, {
   3.7881822932407103`*^9, 3.788182337806439*^9}, 3.788183925636916*^9, 
   3.788183982240913*^9, 3.788184149412444*^9, 3.7881843635938377`*^9, 
   3.788184460410091*^9, 3.788186949989127*^9, 3.788241577696389*^9, 
   3.7882663555011053`*^9, {3.788267387729273*^9, 3.7882673916958*^9}, 
   3.788270571794023*^9, 3.788270916117432*^9, 3.788270957381921*^9, 
   3.788271038949996*^9, {3.7882711237851887`*^9, 3.788271152086294*^9}, 
   3.788271728699988*^9, 3.788271789228167*^9, {3.788271840506049*^9, 
   3.788271868568692*^9}}]
}, Open  ]],

Cell[BoxData["\[IndentingNewLine]"], "Input",
 CellChangeTimes->{3.7881808574356737`*^9}],

Cell[BoxData["\[IndentingNewLine]"], "Input",
 CellChangeTimes->{3.788180856298235*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{"Now", ",", " ", 
    RowBox[{
    "we", " ", "translate", " ", "these", " ", "numbers", " ", "into", " ", 
     "the", " ", "command", " ", "line", " ", "parameters", " ", "of", " ", 
     "JHUGen"}]}], "  ", "*)"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{"(*", "   ", 
   RowBox[{
    RowBox[{
     RowBox[{"copy", " ", "from", " ", 
      RowBox[{"spinzerohiggs_anomcoupl", ".", 
       RowBox[{"f", ":"}]}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"!", 
      RowBox[{
      "anomalous", " ", "couplings", " ", "for", " ", "triple", " ", "and", 
       " ", "quartic", " ", "gauge", " ", "boson", " ", 
       RowBox[{"couplings", ":", "VVV"}]}]}]}], ",", 
    RowBox[{
     RowBox[{"VVVV", "\n", 
      RowBox[{"!", 
       RowBox[{"notation", " ", "wrt", " ", 
        RowBox[{"paper", ":", "dV_N"}]}]}]}], "=", 
     RowBox[{
      RowBox[{"d", "^", 
       RowBox[{"{", "NWW", "}"}]}], "*", "d_", 
      RowBox[{"1", "^", "N"}]}]}], ",", 
    RowBox[{"dP_N", "=", 
     RowBox[{
      RowBox[{"d", "^", 
       RowBox[{"{", "NWW", "}"}]}], "*", "d_", 
      RowBox[{"2", "^", "N"}]}]}], ",", 
    RowBox[{"dM_N", "=", 
     RowBox[{
      RowBox[{"d", "^", 
       RowBox[{"{", "NWW", "}"}]}], "*", "d_", 
      RowBox[{"3", "^", "N"}]}]}], ",", 
    RowBox[{"dFour_N", "=", 
     RowBox[{
      RowBox[{"d", "^", 
       RowBox[{"{", "NWW", "}"}]}], "*", "d_", 
      RowBox[{"4", "^", "N"}]}]}], ",", 
    RowBox[{
     RowBox[{"for", " ", "N"}], "=", "Z"}], ",", 
    RowBox[{
     RowBox[{
      RowBox[{"\\", "gamma"}], "\n", 
      RowBox[{"!", "dZZWpWm"}]}], "=", 
     RowBox[{
      RowBox[{"d", "^", 
       RowBox[{"{", "ZZWW", "}"}]}], "*", 
      RowBox[{
       RowBox[{"sw", "^", "2"}], "/", 
       RowBox[{"cw", "^", "2"}]}]}]}], ",", 
    RowBox[{"dZAWpWm", "=", 
     RowBox[{
      RowBox[{"d", "^", 
       RowBox[{"{", 
        RowBox[{"Z", "\\", "gammaWW"}], "}"}]}], "*", 
      RowBox[{"sw", "/", "cw"}]}]}], ",", 
    RowBox[{"dAAWpWm", "=", 
     RowBox[{"d", "^", 
      RowBox[{"{", 
       RowBox[{"\\", "gamma", "\\", "gammaWW"}], "}"}]}]}]}], 
   "\[IndentingNewLine]", "*)"}], "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.788183154596689*^9, 3.788183202877727*^9}, 
   3.788183246653915*^9}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"dAWW", "=", "1"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"dZWW", "=", 
    RowBox[{"Sqrt", "[", 
     RowBox[{"cw2", "/", "sw2"}], "]"}]}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"JHUGenCoupl", "=", 
   RowBox[{"{", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"dVA", "\[Rule]", "  ", "d1A"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dVZ", "\[Rule]", "  ", "d1Z"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dPA", "\[Rule]", "  ", "d2A"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dPZ", "\[Rule]", "  ", "d2Z"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dMA", "\[Rule]", "  ", "d3A"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dMZ", "\[Rule]", "  ", "d3Z"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dFourA", "\[Rule]", "  ", 
      RowBox[{"dAWW", " ", "d4A"}]}], ",", "\[IndentingNewLine]", 
     RowBox[{"dFourZ", "\[Rule]", "  ", 
      RowBox[{"dAWW", " ", "d4Z"}]}]}], "\[IndentingNewLine]", "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"JHUGenCoupl", " ", "//.", " ", "TGC"}], "//.", " ", 
    "InsertNumbers"}], " ", "//.", " ", "sol1"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"%", "[", 
   RowBox[{"[", "1", "]"}], "]"}], " ", "//", 
  "TableForm"}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.788183251770479*^9, 3.788183260222082*^9}, {
   3.7881838304651413`*^9, 3.788184070258862*^9}, {3.788186925412115*^9, 
   3.788186935651847*^9}, 3.788271087699658*^9, 3.788271878407619*^9}],

Cell[BoxData[
 TagBox[
  TagBox[GridBox[{
     {
      RowBox[{"dVA", "\[Rule]", "0.923119`"}]},
     {
      RowBox[{"dVZ", "\[Rule]", "1.066121492466798`"}]},
     {
      RowBox[{"dPA", "\[Rule]", "1"}]},
     {
      RowBox[{"dPZ", "\[Rule]", "1.0430024924667982`"}]},
     {
      RowBox[{"dMA", "\[Rule]", "1"}]},
     {
      RowBox[{"dMZ", "\[Rule]", "1.0430024924667982`"}]},
     {
      RowBox[{"dFourA", "\[Rule]", "0"}]},
     {
      RowBox[{"dFourZ", "\[Rule]", "0"}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.5599999999999999]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}],
   Column],
  Function[BoxForm`e$, 
   TableForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{
  3.788187032082033*^9, 3.788241577967332*^9, {3.788271082814962*^9, 
   3.788271089471427*^9}, 3.7882717307259502`*^9, {3.78827179352345*^9, 
   3.7882717959238377`*^9}, 3.788271843094035*^9, {3.788271875412665*^9, 
   3.7882718794110403`*^9}}]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.78818703031775*^9, 3.7881870303208437`*^9}}],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.78818793636236*^9, 3.7881879364572363`*^9}}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.7881879414719553`*^9, 3.788188000456118*^9}, 
   3.788241586048854*^9}]
},
WindowSize->{1920, 1135},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
PrintingCopies->1,
PrintingPageRange->{Automatic, Automatic},
PrintingOptions->{"PaperOrientation"->"Portrait",
"PaperSize"->{594.75, 842.25},
"PostScriptOutputFile"->""},
FrontEndVersion->"10.4 for Linux x86 (64-bit) (February 25, 2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 69, 1, 32, "Input"],
Cell[CellGroupData[{
Cell[652, 25, 182, 4, 55, "Input"],
Cell[837, 31, 109, 2, 32, "Output"]
}, Open  ]],
Cell[961, 36, 3000, 54, 561, "Input"],
Cell[CellGroupData[{
Cell[3986, 94, 3364, 81, 231, "Input"],
Cell[7353, 177, 1170, 20, 32, "Output"],
Cell[8526, 199, 1643, 36, 62, "Output"]
}, Open  ]],
Cell[10184, 238, 148, 2, 77, "Input"],
Cell[CellGroupData[{
Cell[10357, 244, 3084, 78, 187, "Input"],
Cell[13444, 324, 577, 12, 32, "Output"],
Cell[14024, 338, 1043, 27, 62, "Output"]
}, Open  ]],
Cell[15082, 368, 146, 2, 77, "Input"],
Cell[CellGroupData[{
Cell[15253, 374, 3415, 85, 187, "Input"],
Cell[18671, 461, 358, 8, 35, "Output"],
Cell[19032, 471, 829, 24, 62, "Output"]
}, Open  ]],
Cell[19876, 498, 194, 3, 32, "Input"],
Cell[20073, 503, 143, 2, 32, "Input"],
Cell[CellGroupData[{
Cell[20241, 509, 2445, 64, 231, "Input"],
Cell[22689, 575, 376, 9, 32, "Output"],
Cell[23068, 586, 835, 23, 46, "Output"]
}, Open  ]],
Cell[23918, 612, 197, 3, 121, "Input"],
Cell[CellGroupData[{
Cell[24140, 619, 4346, 109, 429, "Input"],
Cell[28489, 730, 1730, 46, 172, "Output"]
}, Open  ]],
Cell[30234, 779, 89, 1, 55, "Input"],
Cell[30326, 782, 87, 1, 55, "Input"],
Cell[30416, 785, 2323, 67, 187, "Input"],
Cell[CellGroupData[{
Cell[32764, 856, 1582, 36, 363, "Input"],
Cell[34349, 894, 1243, 37, 140, "Output"]
}, Open  ]],
Cell[35607, 934, 93, 1, 32, "Input"],
Cell[35703, 937, 147, 2, 77, "Input"],
Cell[35853, 941, 120, 2, 32, "Input"]
}
]
*)

