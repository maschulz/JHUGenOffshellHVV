(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     19667,        523]
NotebookOptionsPosition[     18931,        494]
NotebookOutlinePosition[     19269,        509]
CellTagsIndexPosition[     19226,        506]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"InsertNumbers1", "=", 
     RowBox[{"{", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"sw2", "\[Rule]", " ", "0.23119"}], ",", "\[IndentingNewLine]", 
       RowBox[{"cw2", "\[Rule]", " ", 
        RowBox[{"1", "-", "sw2"}]}], ",", "\[IndentingNewLine]", 
       RowBox[{"MZ2", "\[Rule]", " ", 
        RowBox[{"91.19", "^", "2"}]}], ",", "\[IndentingNewLine]", 
       RowBox[{"g2ZZ", "\[Rule]", " ", 
        RowBox[{"0.05", "*", "2"}]}], ",", " ", "\[IndentingNewLine]", 
       RowBox[{"g2AA", "\[Rule]", "0"}], ",", "\[IndentingNewLine]", 
       RowBox[{"g2ZA", "\[Rule]", "0"}], ",", "\[IndentingNewLine]", 
       RowBox[{"Lam1WW", "\[Rule]", " ", "1000"}], ",", "\[IndentingNewLine]", 
       RowBox[{"Lam1ZZ", "\[Rule]", " ", "1000"}], ",", "\[IndentingNewLine]", 
       RowBox[{"Lam1ZA", "\[Rule]", " ", "1000"}], ",", "\[IndentingNewLine]", 
       RowBox[{"kap1ZZ", "\[Rule]", "0"}]}], "\[IndentingNewLine]", "}"}]}], 
    ";"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"InsertNumbers2", "=", 
     RowBox[{"{", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"sw2", "\[Rule]", " ", "0.23119"}], ",", "\[IndentingNewLine]", 
       RowBox[{"cw2", "\[Rule]", " ", 
        RowBox[{"1", "-", "sw2"}]}], ",", "\[IndentingNewLine]", 
       RowBox[{"MZ2", "\[Rule]", " ", 
        RowBox[{"91.19", "^", "2"}]}], ",", "\[IndentingNewLine]", 
       RowBox[{"g2ZZ", "\[Rule]", " ", "0"}], ",", " ", "\[IndentingNewLine]", 
       RowBox[{"g2AA", "\[Rule]", "0"}], ",", "\[IndentingNewLine]", 
       RowBox[{"g2ZA", "\[Rule]", "0"}], ",", "\[IndentingNewLine]", 
       RowBox[{"Lam1WW", "\[Rule]", " ", "1000"}], ",", "\[IndentingNewLine]", 
       RowBox[{"Lam1ZZ", "\[Rule]", " ", "1000"}], ",", "\[IndentingNewLine]", 
       RowBox[{"Lam1ZA", "\[Rule]", " ", "1000"}], ",", "\[IndentingNewLine]", 
       RowBox[{"kap1ZZ", "\[Rule]", 
        RowBox[{
         RowBox[{"-", "5"}], " ", "*", "2"}]}]}], "\[IndentingNewLine]", 
      "}"}]}], ";"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"InsertNumbers", "=", " ", "InsertNumbers2"}], ";"}], 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.788874449110037*^9, 3.788874531542067*^9}, 
   3.788874567000193*^9, {3.7888748079122543`*^9, 3.7888748314317617`*^9}, {
   3.7888751017372026`*^9, 3.788875199882762*^9}, {3.788875280619857*^9, 
   3.7888752819797497`*^9}, {3.7888759096475077`*^9, 3.788875956303327*^9}, {
   3.789119540757491*^9, 3.789119616805732*^9}, {3.7891197508718443`*^9, 
   3.7891197517354193`*^9}, {3.7891197858168163`*^9, 3.789119786727096*^9}, {
   3.789120820829219*^9, 3.789120821932591*^9}, {3.7891284762484293`*^9, 
   3.789128477368327*^9}, {3.789128973415827*^9, 3.7891290186801023`*^9}, {
   3.789129085640925*^9, 3.789129140793357*^9}, {3.789129183994174*^9, 
   3.7891292125864677`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"eq18", "=", 
    RowBox[{"g2WW", "\[Equal]", 
     RowBox[{
      RowBox[{"cw2", " ", "*", " ", "g2ZZ"}], "+", 
      RowBox[{"sw2", " ", "g2AA"}], " ", "+", " ", 
      RowBox[{"2", " ", 
       RowBox[{"Sqrt", "[", 
        RowBox[{"sw2", " ", "*", "cw2"}], "]"}], " ", "g2ZA"}]}]}]}], ";"}], 
  "                                                                           \
                                          "}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"eq20", " ", "=", " ", 
    RowBox[{
     RowBox[{
      RowBox[{"kap1WW", "/", 
       RowBox[{"Lam1WW", "^", "2"}]}], " ", 
      RowBox[{"(", 
       RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", "\[Equal]", " ", 
     RowBox[{
      RowBox[{"kap1ZZ", "/", 
       RowBox[{
        RowBox[{"(", "Lam1ZZ", ")"}], "^", "2"}]}], " ", "+", "  ", 
      RowBox[{"2", " ", "sw2", " ", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"g2AA", "-", "g2ZZ"}], ")"}], "/", "MZ2"}]}], "+", 
      RowBox[{"2", " ", 
       RowBox[{"Sqrt", "[", 
        RowBox[{"sw2", "/", "cw2"}], "]"}], 
       RowBox[{"(", 
        RowBox[{"cw2", "-", "sw2"}], ")"}], " ", 
       RowBox[{"g2ZA", "/", "MZ2"}]}]}]}]}], ";"}], 
  "                                             "}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"eq21", " ", "=", " ", 
    RowBox[{
     RowBox[{
      RowBox[{"kap2ZA", "/", 
       RowBox[{"Lam1ZA", "^", "2"}]}], " ", 
      RowBox[{"(", 
       RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", "\[Equal]", " ", 
     RowBox[{
      RowBox[{"2", " ", 
       RowBox[{"Sqrt", "[", 
        RowBox[{"sw2", " ", "*", " ", "cw2"}], "]"}], "   ", 
       RowBox[{"(", " ", 
        RowBox[{
         RowBox[{"kap1ZZ", "/", 
          RowBox[{
           RowBox[{"(", "Lam1ZZ", ")"}], "^", "2"}]}], " ", "+", "  ", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"g2AA", "-", "g2ZZ"}], ")"}], "/", "MZ2"}]}], " ", ")"}]}],
       " ", "+", " ", 
      RowBox[{"2", " ", 
       RowBox[{"(", 
        RowBox[{"cw2", "-", "sw2"}], ")"}], " ", 
       RowBox[{"g2ZA", "/", "MZ2"}]}]}]}]}], ";"}], 
  "                     "}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"sol1", "=", 
    RowBox[{"Solve", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"eq18", ",", "eq20", ",", "eq21"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"kap1WW", ",", "kap2ZA"}], "}"}]}], "]"}]}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{"sol1", " ", "//.", " ", 
  "InsertNumbers"}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{
  3.7888743326308403`*^9, {3.788874375684442*^9, 3.788874376421032*^9}, {
   3.788874888922761*^9, 3.788875050422585*^9}, {3.788875111737647*^9, 
   3.788875142211843*^9}, {3.789129030808872*^9, 3.7891290309523067`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"kap1WW", "\[Rule]", 
     RowBox[{"-", "18.600498493359623`"}]}], ",", 
    RowBox[{"kap2ZA", "\[Rule]", 
     RowBox[{"-", "15.683703140568353`"}]}]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{{3.788875133518023*^9, 3.788875202918871*^9}, 
   3.788875284020241*^9, {3.7888759121794558`*^9, 3.78887595733924*^9}, 
   3.789119620200191*^9, 3.789119695342763*^9, 3.789119753635273*^9, 
   3.789119788465665*^9, 3.7891200610043373`*^9, {3.789120802580587*^9, 
   3.7891208241043386`*^9}, 3.789121838772189*^9, 3.789128479695553*^9, {
   3.7891289832838507`*^9, 3.789129038632564*^9}, {3.78912907442216*^9, 
   3.789129115702468*^9}, 3.789129146631682*^9, {3.7891291876374063`*^9, 
   3.789129214629829*^9}, 3.7924115701468983`*^9}]
}, Open  ]],

Cell[BoxData["\[IndentingNewLine]"], "Input",
 CellChangeTimes->{{3.788874427973538*^9, 3.788874475418701*^9}, {
   3.788874512646125*^9, 3.788874518549788*^9}, 3.788874748488935*^9}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{
  3.788874546988276*^9, {3.7888745904139*^9, 3.788874598935277*^9}, 
   3.7888748684465*^9}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{3.788874557428335*^9, 3.78887461455296*^9, 
  3.7888748696276207`*^9}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{3.7888746312726393`*^9, 3.7888748708464622`*^9}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"TGC", "=", " ", 
    RowBox[{"{", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"d1A", " ", "\[Rule]", "  ", 
       RowBox[{"1", " ", "+", " ", 
        RowBox[{"cw2", " ", 
         RowBox[{"(", 
          RowBox[{"g2AA", "-", "g2ZZ"}], ")"}]}], "+", " ", 
        RowBox[{"g2ZA", " ", 
         RowBox[{"(", 
          RowBox[{
           RowBox[{"Sqrt", "[", 
            RowBox[{"cw2", "/", "sw2"}], "]"}], "-", 
           RowBox[{"2", " ", 
            RowBox[{"Sqrt", "[", 
             RowBox[{"sw2", " ", "cw2"}], "]"}]}]}], ")"}]}]}]}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"d1Z", " ", "\[Rule]", "  ", 
       RowBox[{"1", "-", 
        RowBox[{"2", " ", "sw2", " ", 
         RowBox[{"cw2", "/", 
          RowBox[{"(", 
           RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
         RowBox[{"(", 
          RowBox[{"g2AA", "-", "g2ZZ"}], ")"}]}], " ", "-", 
        RowBox[{"2", " ", 
         RowBox[{"Sqrt", "[", 
          RowBox[{"sw2", " ", "cw2"}], " ", "]"}], " ", "g2ZA"}], " ", "-", 
        " ", 
        RowBox[{
         RowBox[{
          RowBox[{"MZ2", "/", "2"}], "/", 
          RowBox[{"(", 
           RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
         RowBox[{"kap1ZZ", "/", 
          RowBox[{"Lam1ZZ", "^", "2"}]}]}]}]}], ",", "\[IndentingNewLine]", 
      RowBox[{"d2Z", "\[Rule]", " ", 
       RowBox[{"1", "-", 
        RowBox[{
         RowBox[{"sw2", "/", 
          RowBox[{"(", 
           RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
         RowBox[{"(", 
          RowBox[{"g2AA", "-", "g2ZZ"}], ")"}]}], "-", 
        RowBox[{
         RowBox[{"Sqrt", "[", 
          RowBox[{"sw2", "/", "cw2"}], " ", "]"}], " ", "g2ZA"}], " ", "-", 
        " ", 
        RowBox[{
         RowBox[{
          RowBox[{"MZ2", "/", "2"}], "/", 
          RowBox[{"(", 
           RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
         RowBox[{"kap1ZZ", "/", 
          RowBox[{"Lam1ZZ", "^", "2"}]}]}]}]}], ",", "\[IndentingNewLine]", 
      RowBox[{"d3Z", "\[Rule]", " ", 
       RowBox[{"1", "-", 
        RowBox[{
         RowBox[{"sw2", "/", 
          RowBox[{"(", 
           RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
         RowBox[{"(", 
          RowBox[{"g2AA", "-", "g2ZZ"}], ")"}]}], "-", 
        RowBox[{
         RowBox[{"Sqrt", "[", 
          RowBox[{"sw2", "/", "cw2"}], " ", "]"}], " ", "g2ZA"}], " ", "-", 
        " ", 
        RowBox[{
         RowBox[{
          RowBox[{"MZ2", "/", "2"}], "/", 
          RowBox[{"(", 
           RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
         RowBox[{"kap1ZZ", "/", 
          RowBox[{"Lam1ZZ", "^", "2"}]}]}]}]}], ",", "\[IndentingNewLine]", 
      RowBox[{"dZZWW", "\[Rule]", " ", 
       RowBox[{
        RowBox[{"cw2", "/", "sw2"}], " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"2", " ", 
           RowBox[{"(", 
            RowBox[{"1", "-", 
             RowBox[{
              RowBox[{"sw2", "/", 
               RowBox[{"(", 
                RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
              RowBox[{"(", 
               RowBox[{"g2AA", "-", "g2ZZ"}], ")"}]}], "-", 
             RowBox[{
              RowBox[{"Sqrt", "[", 
               RowBox[{"sw2", "/", "cw2"}], " ", "]"}], " ", "g2ZA"}], " ", 
             "-", " ", 
             RowBox[{
              RowBox[{
               RowBox[{"MZ2", "/", "2"}], "/", 
               RowBox[{"(", 
                RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
              RowBox[{"kap1ZZ", "/", 
               RowBox[{"Lam1ZZ", "^", "2"}]}]}]}], ")"}]}], "-", "1"}], 
         ")"}]}]}], ",", "\[IndentingNewLine]", 
      RowBox[{"dZAWW", "\[Rule]", " ", 
       RowBox[{
        RowBox[{"Sqrt", "[", 
         RowBox[{"cw2", "/", "sw2"}], "]"}], " ", 
        RowBox[{"(", 
         RowBox[{"1", "-", 
          RowBox[{
           RowBox[{"sw2", "/", 
            RowBox[{"(", 
             RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
           RowBox[{"(", 
            RowBox[{"g2AA", "-", "g2ZZ"}], ")"}]}], "-", 
          RowBox[{
           RowBox[{"Sqrt", "[", 
            RowBox[{"sw2", "/", "cw2"}], " ", "]"}], " ", "g2ZA"}], " ", "-", 
          " ", 
          RowBox[{
           RowBox[{
            RowBox[{"MZ2", "/", "2"}], "/", 
            RowBox[{"(", 
             RowBox[{"cw2", "-", "sw2"}], ")"}]}], " ", 
           RowBox[{"kap1ZZ", "/", 
            RowBox[{"Lam1ZZ", "^", "2"}]}]}]}], ")"}]}]}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"dAAWW", " ", "\[Rule]", " ", "1"}], ",", "\[IndentingNewLine]",
       "\[IndentingNewLine]", 
      RowBox[{"d2A", "\[Rule]", " ", "1"}], ",", "\[IndentingNewLine]", 
      RowBox[{"d3A", "\[Rule]", "1"}], ",", "\[IndentingNewLine]", 
      "\[IndentingNewLine]", 
      RowBox[{"d4A", "\[Rule]", "0"}], ",", "\[IndentingNewLine]", 
      RowBox[{"d4Z", "\[Rule]", "0"}]}], "\[IndentingNewLine]", "}"}]}], 
   ";"}], "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"TGC", "  ", "//.", " ", "InsertNumbers"}], " ", "//.", " ", 
   "sol1"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"%", "[", 
   RowBox[{"[", "1", "]"}], "]"}], " ", "//", "TableForm"}]}], "Input",
 CellChangeTimes->{
  3.788874639949153*^9, 3.788874872831341*^9, 3.788875217231353*^9, {
   3.789121149858322*^9, 3.789121194863639*^9}, {3.789121628529654*^9, 
   3.789121794325224*^9}, {3.78912252575179*^9, 3.7891225329515533`*^9}}],

Cell[BoxData[
 TagBox[
  TagBox[GridBox[{
     {
      RowBox[{"d1A", "\[Rule]", "1.`"}]},
     {
      RowBox[{"d1Z", "\[Rule]", "1.0773373023697035`"}]},
     {
      RowBox[{"d2Z", "\[Rule]", "1.0773373023697035`"}]},
     {
      RowBox[{"d3Z", "\[Rule]", "1.0773373023697035`"}]},
     {
      RowBox[{"dZZWW", "\[Rule]", "3.839808741163992`"}]},
     {
      RowBox[{"dZAWW", "\[Rule]", "1.9646115217243416`"}]},
     {
      RowBox[{"dAAWW", "\[Rule]", "1"}]},
     {
      RowBox[{"d2A", "\[Rule]", "1"}]},
     {
      RowBox[{"d3A", "\[Rule]", "1"}]},
     {
      RowBox[{"d4A", "\[Rule]", "0"}]},
     {
      RowBox[{"d4Z", "\[Rule]", "0"}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.5599999999999999]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}],
   Column],
  Function[BoxForm`e$, 
   TableForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{
  3.788875219025745*^9, 3.7888752884693413`*^9, {3.7888759136211643`*^9, 
   3.788875957932399*^9}, 3.788875989508285*^9, 3.789119699070705*^9, {
   3.78911976087764*^9, 3.7891197894836206`*^9}, 3.789120062352409*^9, {
   3.789120807841227*^9, 3.789120826002315*^9}, {3.7891211660881233`*^9, 
   3.789121168018774*^9}, {3.78912180021577*^9, 3.789121802157538*^9}, 
   3.789121838796288*^9, 3.78912253329503*^9, 3.7891284894090633`*^9, {
   3.789129075771297*^9, 3.7891290771374807`*^9}, {3.78912912031844*^9, 
   3.7891291480636044`*^9}, {3.789129189214923*^9, 3.789129215709484*^9}, 
   3.7924115701717787`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"dAWW", "=", "1"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"dZWW", "=", 
    RowBox[{"Sqrt", "[", 
     RowBox[{"cw2", "/", "sw2"}], "]"}]}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"JHUGenCoupl", "=", 
   RowBox[{"{", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"dVA", "\[Rule]", "  ", "d1A"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dVZ", "\[Rule]", "  ", "d1Z"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dPA", "\[Rule]", "  ", "d2A"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dPZ", "\[Rule]", "  ", "d2Z"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dMA", "\[Rule]", "  ", "d3A"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dMZ", "\[Rule]", "  ", "d3Z"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dFourA", "\[Rule]", "  ", 
      RowBox[{"dAWW", " ", "d4A"}]}], ",", "\[IndentingNewLine]", 
     RowBox[{"dFourZ", "\[Rule]", "  ", 
      RowBox[{"dZWW", " ", "d4Z"}]}], ",", "\[IndentingNewLine]", 
     RowBox[{"dZZWpWm", " ", "\[Rule]", " ", 
      RowBox[{
       RowBox[{"sw2", "/", "cw2"}], " ", "dZZWW"}]}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"dZAWpWm", " ", "\[Rule]", " ", 
      RowBox[{
       RowBox[{"Sqrt", "[", 
        RowBox[{"sw2", "/", "cw2"}], "]"}], " ", "dZAWW"}]}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"dAAWpWm", " ", "\[Rule]", " ", "dAAWW"}]}], 
    "\[IndentingNewLine]", "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"JHUGenCoupl", " ", "//.", " ", "TGC"}], "//.", " ", 
    "InsertNumbers"}], " ", "//.", " ", "sol1"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"%", "[", 
   RowBox[{"[", "1", "]"}], "]"}], " ", "//", 
  "TableForm"}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.78912108396666*^9, 3.7891210845101633`*^9}, {
  3.789121843522726*^9, 3.789121843971704*^9}, {3.789122109028858*^9, 
  3.789122208948702*^9}, {3.7891224793444223`*^9, 3.789122503877327*^9}, {
  3.792412081573154*^9, 3.792412081954442*^9}}],

Cell[BoxData[
 TagBox[
  TagBox[GridBox[{
     {
      RowBox[{"dVA", "\[Rule]", "1.`"}]},
     {
      RowBox[{"dVZ", "\[Rule]", "1.0773373023697035`"}]},
     {
      RowBox[{"dPA", "\[Rule]", "1"}]},
     {
      RowBox[{"dPZ", "\[Rule]", "1.0773373023697035`"}]},
     {
      RowBox[{"dMA", "\[Rule]", "1"}]},
     {
      RowBox[{"dMZ", "\[Rule]", "1.0773373023697035`"}]},
     {
      RowBox[{"dFourA", "\[Rule]", "0"}]},
     {
      RowBox[{"dFourZ", "\[Rule]", "0"}]},
     {
      RowBox[{"dZZWpWm", "\[Rule]", "1.154674604739407`"}]},
     {
      RowBox[{"dZAWpWm", "\[Rule]", "1.077337302369703`"}]},
     {
      RowBox[{"dAAWpWm", "\[Rule]", "1"}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.5599999999999999]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}],
   Column],
  Function[BoxForm`e$, 
   TableForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{
  3.7888752640874662`*^9, 3.7888752988585863`*^9, 3.788875915984273*^9, {
   3.789119736198318*^9, 3.7891197369110193`*^9}, {3.789120810131977*^9, 
   3.789120829321951*^9}, {3.7891218112993393`*^9, 3.789121844440748*^9}, {
   3.789122129070621*^9, 3.7891222092344103`*^9}, 3.78912250437965*^9, {
   3.789122535420106*^9, 3.789122537402357*^9}, {3.789128491274856*^9, 
   3.789128492452318*^9}, 3.7891290780050507`*^9, {3.789129122182802*^9, 
   3.789129151018355*^9}, {3.789129191497528*^9, 3.7891292170311537`*^9}, 
   3.79241157018266*^9}]
}, Open  ]]
},
WindowSize->{1920, 1135},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"10.4 for Linux x86 (64-bit) (February 25, 2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 3030, 54, 715, "Input"],
Cell[CellGroupData[{
Cell[3613, 78, 2843, 79, 165, "Input"],
Cell[6459, 159, 801, 15, 32, "Output"]
}, Open  ]],
Cell[7275, 177, 183, 2, 55, "Input"],
Cell[7461, 181, 139, 3, 32, "Input"],
Cell[7603, 186, 116, 2, 32, "Input"],
Cell[7722, 190, 94, 1, 32, "Input"],
Cell[CellGroupData[{
Cell[7841, 195, 5472, 145, 407, "Input"],
Cell[13316, 342, 1781, 48, 202, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15134, 395, 2056, 47, 429, "Input"],
Cell[17193, 444, 1722, 47, 202, "Output"]
}, Open  ]]
}
]
*)

